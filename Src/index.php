<?PHP
	session_start();
	include('includes/links.php');
	//include('footer.html');
	//print_r($_POST);
	$interval = $_POST['interval'];

	$hiddenCodes = $_POST['hiddenCodes'];
	$p_codes = explode(",",$hiddenCodes);

	$hiddenOptions = $_POST['hiddenOptions'];
	$prodOptions = explode(",",$hiddenOptions);

	$level = $_POST['level'];
/*	
	if ($level == "outlet"){
		$HideOutlet="";
		$OutletValue = $_POST['OutletValue'];
	}
	else{
		$HideOutlet = "style=\"display: none;\"";
		$OutletValue = "";
	}
	
	//print $HideOutlet;
*/
	if ($level == "supplier"){
		$HideSupplier="";
		$SupplierValue = $_POST['SupplierName'];
	}
	else{
		$HideSupplier = "style=\"display: none;\"";
		$SupplierValue = "";
	}
	
	$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	if(strpos($user_agent, 'MSIE') !== false)$isIE=true;
	set_time_limit(300);
?>

<HTML>
<HEAD>
	<script language="javaScript" type="text/javascript" src="javascript.js"></script>
	<!--<script language="javaScript" type="text/javascript" src="products.js.php"></script>-->
	<link href="style.css" rel="stylesheet" type="text/css" media="screen">
	<TITLE>Food Delivery Reporting For Whitbread Supply Chain</TITLE>
</HEAD>
<BODY onLoad="prepare()"><!-- background="images/background.gif">-->
<DIV ID="container">
	<DIV ID="intervalSelection">
	<!-- This table structures the page's title and link -->
	<table width="100%">
		<tr>
			<td align=center>
				<img src="images/logo.gif" align=top alt="Whitbread logo">
				<br><br>
				<a href="manage.php">Heirarchy Management</a>
			</td>
			<td align=center>
				<h1>Food Delivery Reporting</h1>
				<h2>Reporting Page</h2>
			</td>
		</tr>
	</table>
	<p>
	<center><img src="images/divider.gif" alt="blue divider"></center><p>
	<TABLE CELLPADDING=5 WIDTH="100%">
	<FORM NAME="form1" method="POST" action="index.php">

	<!-- Made table more complex to aid improved layout. -->
	<TR title="Select a date range between 2 week-ending dates">
		<TD ALIGN="LEFT">
			<INPUT <?PHP if((!isset($interval)) || $interval=="week") echo "CHECKED" ?> TYPE="Radio" NAME="interval" VALUE="week" ID="week" ONCLICK="disableIntervals()">
			<LABEL FOR="week">Week</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="startWeek">From Week Ending:</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="startWeek" ID="startWeek">
				<?PHP
				// Get all applicable Weeks From DB view, viewLast18Months
				$sql = "SELECT Week_Name FROM viewLast18Months ORDER BY Week_Seq DESC";
				$result = odbc_exec($link, $sql);
				$weeks="";
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Week_Name");
					$weeks .= "<OPTION VALUE=\"".$val."\">".$val."</OPTION>\n";
				}
				echo $weeks;
				?>
			</SELECT>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="endWeek">Until Week Ending: </LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="endWeek" ID="endWeek">
				<?PHP
					echo $weeks;
				?>
			</SELECT>
		</TD>
	</TR>

	<TR title="Select a date range between 2 period-ending dates">
		<TD ALIGN="LEFT">
			<INPUT <?PHP if($interval=="period") echo "CHECKED" ?> TYPE="Radio" NAME="interval" VALUE="period" ID="period" ONCLICK="disableIntervals()">
			<LABEL FOR="period">Period</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="startPeriod">From Period Ending:</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="startPeriod" ID="startPeriod">
				<?PHP
				// Get all applicable Periods from viewLast18Months
				$sql = "SELECT DISTINCT Period_Seq, Convert(Varchar(10),MAX(CONVERT(DateTime,Week_Name,103)),103) as Period_End FROM viewLast18Months GROUP BY Period_Seq ORDER BY Period_Seq DESC";
				$result = odbc_exec($link, $sql);
				$periods = "";
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Period_Seq");
					$text = odbc_result($result, "Period_End");
					$periods .= "<OPTION VALUE=".$val.">".$text."</OPTION>\n";
				}
				echo $periods;
				?>
			</SELECT>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="endPeriod">Until Period Ending:</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="endPeriod" ID="endPeriod">
				<?PHP
					echo $periods;
				?>
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 ALIGN="CENTER">
			<IMG SRC="images/divider.gif">
		</TD>
	</TR>
	<TR title="Select the level the data will group to">
		<TD></TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="level">Aggregation Level: </LABEL>
		</TD>
		<TD>
			<SELECT NAME="level" ID="level" ONCHANGE="toggleSupplierInput()">
				<OPTION <?PHP if ($level=="brand")echo "SELECTED";?> VALUE="brand">Brand</OPTION>
				<OPTION <?PHP if ($level=="concept")echo "SELECTED";?> VALUE="concept">Concept</OPTION>
				<OPTION <?PHP if ($level=="subconcept")echo "SELECTED";?> VALUE="subconcept">Sub-Concept</OPTION>
				<OPTION <?PHP if ($level=="outlet")echo "SELECTED";?> VALUE="outlet">Outlet</OPTION>
				<OPTION <?PHP if ($level=="supplier")echo "SELECTED";?> VALUE="supplier">Supplier Report</OPTION>
				<OPTION <?PHP if ($level=="costaorder")echo "SELECTED";?> VALUE="costaorder">Costa Order Summary</OPTION>
			</SELECT>
			<SELECT ID="SupplierName" NAME="SupplierName" <?PHP echo $HideSupplier ?>>
				<?PHP 
					$supplier_sql = "EXEC procGet_Suppliers";
					$supplier_results = odbc_exec($link, $supplier_sql);
					while (odbc_fetch_row($supplier_results))
					{
						$SupplierName = odbc_result($supplier_results, "Supplier");
						echo '<OPTION VALUE="'.$SupplierName.'" '.($SupplierName==$_POST['SupplierName']?"SELECTED":"").'>'.$SupplierName.'</OPTION>';
					}
				?>
			</SELECT>
			<!--<INPUT TYPE="text" ID="OutletValue" NAME="OutletValue" MAXLENGTH="10" Value="<?PHP echo $OutletValue ?>" <?PHP echo $HideOutlet ?> />-->
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 ALIGN="CENTER">
			<IMG SRC="images/divider.gif">
		</TD>
	</TR>
	<TR title="Select whether to show all products or enter desired product codes and whether to display results grouped by day, not week">
		<TD COLSPAN=2 ALIGN="CENTER">
			<LABEL for="showAll">Show All Products</LABEL>
			<INPUT <?PHP if($p_codes[0] == '') echo "CHECKED";?> TYPE="checkbox" VALUE=1 ID="showAll" name="showAll" onClick="selectAll()">
			<P>
			<LABEL for="critical">Show Results By Day</LABEL>
			<INPUT <?PHP if($_POST['critical']==1) echo "CHECKED";?> TYPE="checkbox" VALUE=1 ID="critical" name="critical">
			<!--<p>
			<input type="text" id="code" name="code" value="">
			<input type="button" id="addCode" name="addCode" value="Add Code" onclick="addCodes(get('code').value)">
			<p>-->
		</td>
		<td colspan=2 align="center">
			<select multiple size=5 id="codeList" name="codeList" style="width:300px">
			<?PHP
				if ($prodOptions[0]<>''){
					foreach ($prodOptions as $p){
						echo "<option value=".$p.">".$p."</option>";
					}
				}
			?>
			</select><p>
			<input type="hidden" id="hiddenCodes" name="hiddenCodes" value="<?PHP echo $hiddenCodes?>">
			<input type="hidden" id="hiddenOptions" name="hiddenOptions" value="<?PHP echo $hiddenOptions?>">
			<input type="button" id="addCode" name="addCode" value="Add Codes" onclick="window.open('codes.php','codes','toolbar=no, menubar=no, status=no,location=no,scrollbars=yes,height=200, width=550')">
			<input type="button" id="remCode" name="remCode" value="Remove Selected Codes" onclick="remCodes()">
		</TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 ALIGN="CENTER">
			<IMG SRC="images/divider.gif">
		</TD>
	</TR>
	<tr><td colspan=5 align="center">
		<INPUT TYPE="button" ID="retrieve" value="Retrieve Data" onClick="validate()">
		<!--INPUT TYPE="button" ID="exportsupplier" VALUE="Export Supplier Report" ONCLICK="validate()" <?PHP echo $HideSupplier?>>-->
	</td></tr>
	</FORM></TABLE>
	</DIV>

	<!-- This Javascript block processes the newly defined form above. -->
	<script language="javaScript" type="text/javascript">
	<?PHP
		//Apply previously submitted values to the current page.
		/*if($p_codes[0] <> '')
			foreach($p_codes as $code)
				echo "addCodes(".$code.");\n";*/

		if($interval=="week")
		{
			echo "selectOption('startWeek', '".$_POST['startWeek']."');\n";
			echo "selectOption('endWeek', '".$_POST['endWeek']."');\n";
		}
		if($interval=="period")
		{
			echo "selectOption('startPeriod', '".$_POST['startPeriod']."');\n";
			echo "selectOption('endPeriod', '".$_POST['endPeriod']."');\n";
		}
	?>
	</script>
	<DIV ID="resultsDiv"
		<?PHP
			if (!array_key_exists("level", $_POST))echo " style=display:none";
			//if ($isIE==TRUE) echo " style=height:240px;"
		?>
	>
		<center><!--<TABLE ALIGN="CENTER" WIDTH=100%>

		<TR><TD COLSPAN=2 ALIGN="CENTER" ID="resultsPane">-->
<?PHP
if (array_key_exists("level", $_POST)){
	$interval = $_POST['interval'];
	$showAll = $_POST['showAll'];
	$hiddenCodes = $_POST['hiddenCodes'];
	$level = $_POST['level'];
	if ($_POST['critical']==0)
	{
		if ($interval == "week")
			$grouping = "Week_Ending";
		if ($interval == "period")
			$grouping = "Period_Code";
	}
	else
		$grouping = "Date_Delivered";

	// Get the delivery date ranges
	if ($interval == "week"){
		$startDate = $_POST['startWeek'];
		$endDate = $_POST['endWeek'];
		$where = "DeliveryDate BETWEEN DateAdd(dd,-6,CONVERT(datetime,'".$startDate."',103)) AND CONVERT(datetime,'".$endDate."',103)";
	}else{
		$startPeriod = $_POST['startPeriod'];
		$endPeriod = $_POST['endPeriod'];

//		$startDate = "(SELECT TOP 1 Week_Name FROM viewLast18Months WHERE Period_Seq =".$startPeriod." ORDER BY CONVERT(DATETIME,Week_Name,103) ASC)";
//		$endDate = "(SELECT TOP 1 Week_Name FROM viewLast18Months WHERE Period_Seq =".$endPeriod." ORDER BY CONVERT(DATETIME,Week_Name,103) DESC)";
		$date_range_sql = "SELECT CONVERT(VARCHAR(20), MIN(DATEADD(dd, -6, CONVERT(DATETIME,Week_Name,103))), 103) as Start_Date, CONVERT(VARCHAR(20), MAX(CONVERT(DATETIME,Week_Name,103)), 103) as End_Date FROM viewLast18Months WHERE Period_Seq IN (".$startPeriod.", ".$endPeriod.")";
		$date_range_results = odbc_exec($link, $date_range_sql);
		while (odbc_fetch_row($date_range_results))
		{
			$startDate = odbc_result($date_range_results, "Start_Date");
			$endDate = odbc_result($date_range_results, "End_Date");
		}
		$where = "DeliveryDate BETWEEN CONVERT(datetime,'".$startDate."',103) AND CONVERT(datetime,'".$endDate."',103)";
	}

	/**
	 * Construct the SQL queries to retrieve the aggregation.
	 *
	 * This uses a derived table (ingeniously called derivedTable) due to the use of a subquery to get "week_commencing"
	 * as it could not be included in the group by clause and that defeated the whole process of the aggregation
	 *
	 * The column names for concepts are generated using a PHP loop (so names are COL1,COL2...COLX) rather than the concept codes
	 * found in the SOD data. The reason for this is there is a code 'IN' which is a protected word.
	 */
	if ($level != 'supplier' && $level != 'costaorder'){
	
		$term = '';
		if ($level=="concept"){
			$term = 'Concept';
		}
		elseif ($level=="subconcept"){
			$term = 'SubConcept';
		}elseif ($level == "outlet"){
			$term = 'Outlet';
		}
		if ($term <> ''){
			//$codes_sql = "SELECT * FROM tbl_CodeTranslation";
			if ($level <> "outlet")
				$codes_sql = "SELECT DISTINCT ".$term."Code FROM tbl_".$term."s WHERE ".$term."Code <> '<N/A>'";
				//$codes_sql = "SELECT * FROM tbl_".$term."s WHERE ".$term."Code <> '<N/A>'";
			else
				$codes_sql = "SELECT DISTINCT [OutletCode] FROM tbl_Hierarchy WHERE OutletCode";
			$codes_sql_results = odbc_exec($link, $codes_sql);
			$sql = "SELECT ".$grouping.", ".($level=="outlet"?"SubConceptCode [Sub Concept], [Site],Supplier,":"")."WBCode, Sum(Total) As Total";
	
			$index = 0;
	
			while (odbc_fetch_row($codes_sql_results)==TRUE){
				//$value = odbc_result($codes_sql_results, "SOD_ConceptCode");
				$value = odbc_result($codes_sql_results, "".$term."Code");
				$sql .= ",SUM(COL".$index.") As '".$value."'";
				$index++;
			}
	
			$sql .= " FROM (SELECT";
			if($grouping == "Week_Ending")
				$sql .= " Calendar.Week_Name AS Week_Ending,";
			elseif($grouping == "Period_Code")
				$sql .= " Calendar.Period_Code AS Period_Code,";
	//			$sql .= " CONVERT(VARCHAR(20), MAX(CONVERT(DATETIME, Calendar.Week_Name, 103)), 103) AS Period_Code,";
			else
				$sql .= " CONVERT(VARCHAR(10),data.DeliveryDate,103) As Date_Delivered,";
			
			if ($level == 'outlet')
				$sql .="SubConceptCode, h.OutletCode + ' / ' + h.OutletCode8 + ' / ' + h. OutletName [Site], Supplier, data.WBCode + ' - ' + dc.description [WBCode], SUM(data.Delivered) AS Total";
			else	
		    	$sql .= "data.WBCode, SUM(data.Delivered) AS Total";
	
		    if ($level=='outlet'){
			    $headers .= "From ".$grouping." ".$startDate." To ".$grouping." ".$endDate."\n\r";
			    $headers .= "Sub Concept,".$term.",Supplier, WBCode,Total";
		    }
		    else $headers = $grouping.",".$term.", WBCode,Total";
	//echo $headers;
		    odbc_fetch_row ($codes_sql_results ,0);
		    $index=0;
		    while (odbc_fetch_row($codes_sql_results)==TRUE){
				//$val = odbc_result($codes_sql_results, "SOD_ConceptCode");
				$val = odbc_result($codes_sql_results, "".$term."Code");
				//$sql .= ",SUM(CASE WHEN lup.[SOD_ConceptCode] = '".$val."' THEN Delivered ELSE 0 END) AS 'COL".$index."'";
				$sql .= ",SUM(CASE WHEN con.[".$term."Code] = '".$val."' THEN Delivered ELSE 0 END) AS 'COL".$index."'";
				$headers .= ",".$val;
				$index++;
			}
	
			//$sql .= " FROM dbo.tbl_Concepts AS con INNER JOIN";
			//$sql .= " dbo.tbl_CodeTranslation AS lup ON lup.ConceptCode = con.ConceptCode LEFT OUTER JOIN";
			//$sql .= " dbo.tbl_SOD_Data AS data ON lup.SOD_ConceptCode = data.Concept";
	
			if ($level=="subconcept"){
				$sql .= " FROM dbo.viewSubconcepts AS con";
				$sql .= " INNER JOIN dbo.tbl_SOD_Data AS data ON con.OutletCode = data.Outlet";
			}elseif($level == "outlet"){
				$sql .= " FROM dbo.tbl_SOD_Data AS data";
				$sql .= " INNER JOIN dbo.tbl_Hierarchy h";
				$sql .= " ON data.Outlet = h.OutletCode";
				$sql .= " INNER JOIN dbo.tbl_dishcost dc ON data.WBCode = dc.WBCode AND h.ConceptCode = dc.Concept";
			}else{
				//$sql .= " FROM dbo.tbl_".$term."s AS con INNER JOIN";
				$sql .= " FROM dbo.tbl_Hierarchy AS con INNER JOIN";
				$sql .= " dbo.tbl_SOD_Data AS data ON con.OutletCode = data.Outlet";
			}
			$sql .= " INNER JOIN dbo.Calendar on data.DeliveryDate BETWEEN DATEADD(dd, -6, CONVERT(datetime, Calendar.Week_Name, 103)) AND CONVERT(datetime, Calendar.Week_Name, 103) ";
	
			$sql .= " WHERE (data.WBCode IS NOT NULL) AND (data.DeliveryDate IS NOT NULL) AND ";
			/*if ($level == "outlet")
				$sql .= "Outlet = '".$_POST['OutletValue']."' AND ";*/
		}else{
			//$codes_sql = "SELECT distinct BrandCode FROM tbl_CodeTranslation";
			$codes_sql = "SELECT * FROM tbl_Brands WHERE BrandCode <> '<N/A>'";
			$codes_sql_results = odbc_exec($link, $codes_sql);
	
			$sql = "SELECT ".$grouping.", WBCode,Sum(Total) As Total";
	
			while (odbc_fetch_row($codes_sql_results)==TRUE){
				$value = odbc_result($codes_sql_results, "BrandCode");
				$sql .= ",SUM(".$value.") As ".$value;
			}
	
			$sql .= " FROM (SELECT";
		    if ($grouping == "Week_Ending")
		    	$sql .= " Calendar.Week_Name AS Week_Ending,";
			elseif($grouping == "Period_Code")
				$sql .= " Calendar.Period_Code,";
	//			$sql .= " CONVERT(VARCHAR(20), MAX(CONVERT(DATETIME, Calendar.Week_Name, 103)), 103) AS Period_Code,";
		    else
		    	$sql .= " CONVERT(VARCHAR(10),data.DeliveryDate,103) As Date_Delivered,";
		    $sql .= "data.WBCode, SUM(data.Delivered) AS Total";
	
		    $headers = $grouping.",WBCode,Total"; // column headers for the CSV file
		    odbc_fetch_row ($codes_sql_results ,0); // returns the pointer to the beginning of the array
	
		    while (odbc_fetch_row($codes_sql_results)==TRUE){
				$val = odbc_result($codes_sql_results, "BrandCode");
				$sql .= ",SUM(CASE WHEN [BrandCode] = '".$val."' THEN Delivered ELSE 0 END) AS '".$val."'";
				$headers .= ",".$val;
			}
	
			//$sql .= " FROM dbo.tbl_Brands AS brd INNER JOIN";
			//$sql .= " dbo.tbl_Concepts as con ON con.BrandCode = brd.BrandCode INNER JOIN";
			//$sql .= " dbo.tbl_SOD_Data AS data ON con.ConceptCode = data.Concept";
			$sql .= " FROM dbo.tbl_SOD_Data AS data INNER JOIN Tbl_Hierarchy h ON h.OutletCode = data.Outlet";
			$sql .= " INNER JOIN dbo.Calendar on data.DeliveryDate BETWEEN DATEADD(dd, -6, CONVERT(datetime, Calendar.Week_Name, 103)) AND CONVERT(datetime, Calendar.Week_Name, 103) ";
			$sql .= " WHERE (data.WBCode IS NOT NULL) AND (data.DeliveryDate IS NOT NULL) AND ";
		}
	
		if ($showAll != 1 && $hiddenCodes !="") {
			// add the checked product codes to where clause
			$i = 0;
			$where .= " AND (data.WBCode IN (";
			while ($i < sizeof($p_codes)-1){
				$val = $p_codes[$i];
				$where .= "'".$val."',";
				$i++;
			}
			$val = $p_codes[$i];
			$where .= "'".$val."'))";
		}
	
		$sql .= $where;
		$sql .= " GROUP BY ";
	    if ($grouping == "Week_Ending")
	    	$sql .= " Calendar.Week_Name,";
		elseif($grouping == "Period_Code")
			$sql .= " Calendar.Period_Code,";
		else
			$sql .= " data.DeliveryDate,";
		if ($level == "outlet")
			$sql.= "OutletCode, OutletCode8, OutletName, dc.Description, SubConceptCode, Supplier,";
		$sql .= " data.WBCode) derivedTable";
		$sql .= " GROUP BY ".$grouping.", WBCode";
		if ($level == 'outlet'){
			$sql .= ",SubConceptCode, [Site], Supplier";
			$sql .= " ORDER BY [Site],CONVERT(datetime,".$grouping.",103),WBCode";
		}else	$sql .= " ORDER BY CONVERT(datetime,".$grouping.",103),WBCode";
		
		//print $sql;
		//die();
	}else{
		if ($level == 'supplier'){
			/* Run the supplier Report */
			$SupplierName = $_POST['SupplierName'];
			$pByDay = 0;
			if (isset($_POST['critical']))
				$pByDay = $_POST['critical'];
			
			if ($showAll != 1 && $hiddenCodes !="") {
				// add the checked product codes to where clause
				$i = 0;
				while ($i < sizeof($p_codes)-1){
					$val = $p_codes[$i];
					$wbcodes .= $val.",";
					$i++;
				}
				$val = $p_codes[$i];
				$wbcodes .= $val;
			}
			
			if ($interval == 'week'){
				$StartDate = $_POST['startWeek'];
				$EndDate = $_POST['endWeek'];
			}else{
				$StartDate = $_POST['startPeriod'];
				$EndDate = $_POST['endPeriod'];
			}
			
			$sql = 'EXEC procGet_Supplier_Report @pSupplier = \''.$SupplierName.'\', @pInterval = \''.$interval.'\', @pStartDate = \''.$StartDate.'\', @pEndDate = \''.$EndDate.'\', @pShowByDay = '.$pByDay;
			//print $sql;
			//die();
			if ($wbcodes != ''){
				echo $wbcodes == '';
				$sql .= ', @pWBCodes = \''.$wbcodes.'\'';
			}
		}elseif($level == 'costaorder'){
			//echo "co 1";
			if ($showAll != 1 && $hiddenCodes !="") {
				// add the checked product codes to where clause
				$i = 0;
				while ($i < sizeof($p_codes)-1){
					$val = $p_codes[$i];
					$wbcodes .= $val.",";
					$i++;
				}
				$val = $p_codes[$i];
				$wbcodes .= $val;
			}
			//echo "co 2";
			if ($interval == 'week'){
				$StartDate = $_POST['startWeek'];
				$EndDate = $_POST['endWeek'];
			}else{
				$StartDate = $_POST['startPeriod'];
				$EndDate = $_POST['endPeriod'];
			}
			//echo "co 3";
			$sql = 'EXEC procGet_Costa_Order_Summary @pInterval = \''.$interval.'\', @pStartDate = \''.$StartDate.'\', @pEndDate = \''.$EndDate.'\'';
			if ($wbcodes != ''){
				//echo $wbcodes == '';
				$sql .= ', @pWBCodes = \''.$wbcodes.'\'';
			}
			//echo $sql;
			//die();
			
		}
	}
	//echo $sql;
//die();
	$result = odbc_exec($link, $sql);
	$rowCount = odbc_result_all($result, 'id="Search_Results" name="Search_Results"' . (($level == "supplier")||($level == "costaorder")?"style=\"display:none;\"":""));
	if ($level == 'supplier' || $level == 'costaorder'){
		if ($rowCount > 0) echo '<span style="font-weight:bold; font-size:1.2em;">Click "Export Data" to save results to a CSV file</span>';
		//else echo '<span style="font-weight:bold; font-size:1.2em;">No Data Found</span>';
		//echo $rowCount;
	}
	odbc_fetch_row ($result ,0);
	
	/*
	if ($level != 'supplier'){
		odbc_result_all($result, 'id="Search_Results" name="Search_Results"');
		odbc_fetch_row ($result ,0);
	}else{
		//$result_array = odbc_fetch_array($result);
		//print_r($result_array);
		//$numRows = count($result_array);
		echo '<table id="Search_Results" name="Search_Results"><tr><td>';
		echo $numRows.' Rows found. Click "Export Data" to save results to a CSV File</td></tr></table>';
	}*/
	
	$result = odbc_exec($link, $sql);
	if (($level == 'supplier' || $level == 'costaorder') && $rowCount > 0){
		$result_array = odbc_fetch_array($result);
		$result_array = array_keys($result_array);
		$headers = implode($result_array,",");
		$_SESSION['Supplier'] = str_replace(' ','_',$SupplierName);
	}
	
	$_SESSION['headers'] = $headers;
	$_SESSION['query'] = $sql;
	$_SESSION['level'] = $level;
	
	//echo $_SESSION['query'];
	
	$numRows = odbc_num_rows($result);
	
	/*$csv_data = $headers . " \n"; // write the headers to a variable that will exported to the csv file
	$numRows = odbc_num_rows($result);

	for($i=0; $i<$numRows; $i++){
		$csv_data .= implode(odbc_fetch_array($result),",") . " \n"; // created comma-delimited rows for each of the result rows
	}

	$_SESSION['csv_data'] = $csv_data; // Save the CSV information to a session variable so it can be accessed in download.php*/
	if (($isIE==TRUE)&& ($numRows >0)) echo " <script language=\"javaScript\" type=\"text/javascript\">resHeight();</script>";


}
?>
		</center>
	</DIV>

	<DIV ID="bottomDiv"<?PHP if (!array_key_exists("level", $_POST))echo " style=display:none"?>>
		<TABLE WIDTH=100% HEIGHT=100%>
		<TR CLASS="controls">
		<TD ALIGN="CENTER"><!--<INPUT TYPE="checkbox" ID="autostart" name="autostart"><label for="autostart">Autostart Excel</label>&nbsp-->
		<INPUT TYPE="button" VALUE="Export Data" ONCLICK="window.location='download.php'">
		</TD>
		<TD ALIGN="CENTER"><INPUT TYPE="button" value="Clear Results" onClick="clearResults()"></TD></TR>
		<!-- <INPUT TYPE="button" value="Exit"> -->
		</TABLE>
	</DIV>
</DIV>
</BODY>
</HTML>
