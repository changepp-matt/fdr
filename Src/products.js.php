<?PHP
	include('includes/links.php');

	//Find all WBCode, Description combinations and echo the results into JS function fillProdCodes() to Populate JS array
	$sql = "SELECT DISTINCT WBCode,Description FROM ViewWBCodes ORDER BY WBCode";
	$codes_res = odbc_exec($link, $sql);
	while (odbc_fetch_row($codes_res)==TRUE){
		$code = odbc_result($codes_res, "WBCode");
		$desc = odbc_result($codes_res, "Description");
		if (!strchr($desc,'""')==FALSE){
			$desc = str_replace('""','inch',$desc);
			$desc = str_replace('"','',$desc);
		}
		echo "productCodes[productCodes.length] = new Array(\"" .$code. "\" , \"" .$desc. "\");\n";
	}
?>