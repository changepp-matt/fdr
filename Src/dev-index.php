<?PHP
	session_start();
	include('includes/links.php');
	//include('footer.html');
	//print_r($_POST);
	$interval = $_POST['interval'];
	
	$hiddenCodes = $_POST['hiddenCodes'];
	$p_codes = explode(",",$hiddenCodes);
	
	$hiddenOptions = $_POST['hiddenOptions'];
	$prodOptions = explode(",",$hiddenOptions);
	
	$level = $_POST['level'];
	
	$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';  
	if(strpos($user_agent, 'MSIE') !== false)$isIE=true;
	set_time_limit(300);
?>

<HTML>
<HEAD>
	<script language="javaScript" type="text/javascript" src="javascript.js"></script>
	<!--<script language="javaScript" type="text/javascript" src="products.js.php"></script>-->
	<link href="style.css" rel="stylesheet" type="text/css" media="screen">
	<TITLE>Food Delivery Reporting For Whitbread Supply Chain</TITLE>
</HEAD>
<BODY onLoad="prepare()"><!-- background="images/background.gif">-->
<DIV ID="container">
	<DIV ID="intervalSelection">
	<!-- This table structures the page's title and link --> 
	<table width="100%">
		<tr>
			<td align=center>
				<img src="images/logo.gif" align=top alt="Whitbread logo">
				<br><br>
				<a href="manage.php">Heirarchy Management</a>
			</td>
			<td align=center>
				<h1>Food Delivery Reporting</h1>
				<h2>Reporting Page</h2>
			</td>
		</tr>
	</table>
	<p>
	<center><img src="images/divider.gif" alt="blue divider"></center><p>
	<TABLE CELLPADDING=5 WIDTH="100%">
	<FORM NAME="form1" method="POST" action="dev-index.php">
	
	<!-- Made table more complex to aid improved layout. -->
	<TR title="Select a date range between 2 week-ending dates">
		<TD ALIGN="LEFT">
			<INPUT <?PHP if((!isset($interval)) || $interval=="week") echo "CHECKED" ?> TYPE="Radio" NAME="interval" VALUE="week" ID="week" ONCLICK="disableIntervals()">
			<LABEL FOR="week">Week</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="startWeek">From Week Ending:</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="startWeek" ID="startWeek">
				<?PHP
				// Get all applicable Weeks From DB view, viewLast18Months
				$sql = "SELECT Week_Name FROM viewLast18Months ORDER BY Week_Seq DESC";
				$result = odbc_exec($link, $sql);
				$weeks="";
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Week_Name");
					$weeks .= "<OPTION VALUE=\"".$val."\">".$val."</OPTION>\n";
				}
				echo $weeks;
				?>
			</SELECT>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="endWeek">Until Week Ending: </LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="endWeek" ID="endWeek">
				<?PHP 
					echo $weeks;
				?>
			</SELECT>
		</TD>
	</TR>
	
	<TR title="Select a date range between 2 period-ending dates">
		<TD ALIGN="LEFT">
			<INPUT <?PHP if($interval=="period") echo "CHECKED" ?> TYPE="Radio" NAME="interval" VALUE="period" ID="period" ONCLICK="disableIntervals()">
			<LABEL FOR="period">Period</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="startPeriod">From Period Ending:</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="startPeriod" ID="startPeriod">
				<?PHP
				// Get all applicable Periods from viewLast18Months
				$sql = "SELECT DISTINCT Period_Seq, Convert(Varchar(10),MAX(CONVERT(DateTime,Week_Name,103)),103) as Period_End FROM viewLast18Months GROUP BY Period_Seq ORDER BY Period_Seq DESC";
				$result = odbc_exec($link, $sql);
				$periods = "";
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Period_Seq");
					$text = odbc_result($result, "Period_End");
					$periods .= "<OPTION VALUE=".$val.">".$text."</OPTION>\n";
				}
				echo $periods;
				?>
			</SELECT>
		</TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="endPeriod">Until Period Ending:</LABEL>
		</TD>
		<TD ALIGN="LEFT">
			<SELECT NAME="endPeriod" ID="endPeriod">
				<?PHP 
					echo $periods;
				?>
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 ALIGN="CENTER">
			<IMG SRC="images/divider.gif">
		</TD>
	</TR>
	<TR title="Select the level the data will group to">
		<TD></TD>
		<TD ALIGN="LEFT">
			<LABEL FOR="level">Aggregation Level: </LABEL>
		</TD>
		<TD>
			<SELECT NAME="level" ID="level">
				<OPTION <?PHP if ($level=="brand")echo "SELECTED";?> VALUE="brand">Brand</OPTION>
				<OPTION <?PHP if ($level=="concept")echo "SELECTED";?> VALUE="concept">Concept</OPTION>
				<OPTION <?PHP if ($level=="subconcept")echo "SELECTED";?> VALUE="subconcept">Sub-Concept</OPTION>
				<!--<OPTION <?PHP if ($level=="outlet")echo "SELECTED";?> VALUE="outlet">Outlet</OPTION>-->
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 ALIGN="CENTER">
			<IMG SRC="images/divider.gif">
		</TD>
	</TR>
	<TR title="Select whether to show all products or enter desired product codes and whether to display results grouped by day, not week">
		<TD COLSPAN=2 ALIGN="CENTER">
			<LABEL for="showAll">Show All Products</LABEL>
			<INPUT <?PHP if($p_codes[0] == '') echo "CHECKED";?> TYPE="checkbox" VALUE=1 ID="showAll" name="showAll" onClick="selectAll()">
			<P>
			<LABEL for="critical">Show Results By Day</LABEL>
			<INPUT <?PHP if($_POST['critical']==1) echo "CHECKED";?> TYPE="checkbox" VALUE=1 ID="critical" name="critical">
			<!--<p>
			<input type="text" id="code" name="code" value="">
			<input type="button" id="addCode" name="addCode" value="Add Code" onclick="addCodes(get('code').value)">
			<p>-->
		</td>
		<td colspan=2 align="center">
			<select multiple size=5 id="codeList" name="codeList" style="width:300px">
			<?PHP
				if ($prodOptions[0]<>''){
					foreach ($prodOptions as $p){
						echo "<option value=".$p.">".$p."</option>";
					}
				}
			?>
			</select><p>
			<input type="hidden" id="hiddenCodes" name="hiddenCodes" value="<?PHP echo $hiddenCodes?>">
			<input type="hidden" id="hiddenOptions" name="hiddenOptions" value="<?PHP echo $hiddenOptions?>">
			<input type="button" id="addCode" name="addCode" value="Add Codes" onclick="window.open('codes.php','codes','toolbar=no, menubar=no, status=no,location=no,scrollbars=yes,height=200, width=550')">
			<input type="button" id="remCode" name="remCode" value="Remove Selected Codes" onclick="remCodes()">
		</TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 ALIGN="CENTER">
			<IMG SRC="images/divider.gif">
		</TD>
	</TR>
	<tr><td colspan=5 align="center"><INPUT TYPE="button" value="Retrieve Data" onClick="validate()"></td></tr>
	</FORM></TABLE>
	</DIV>
	
	<!-- This Javascript block processes the newly defined form above. -->
	<script language="javaScript" type="text/javascript">
	<?PHP
		//Apply previously submitted values to the current page.
		/*if($p_codes[0] <> '')
			foreach($p_codes as $code)
				echo "addCodes(".$code.");\n";*/
				
		if($interval=="week")
		{
			echo "selectOption('startWeek', '".$_POST['startWeek']."');\n";
			echo "selectOption('endWeek', '".$_POST['endWeek']."');\n";
		}
		if($interval=="period")
		{
			echo "selectOption('startPeriod', '".$_POST['startPeriod']."');\n";
			echo "selectOption('endPeriod', '".$_POST['endPeriod']."');\n";
		}
	?>
	</script>
	<DIV ID="resultsDiv"
		<?PHP
			if (!array_key_exists("level", $_POST))echo " style=display:none";
			//if ($isIE==TRUE) echo " style=height:240px;"
		?>
	>
		<center><!--<TABLE ALIGN="CENTER" WIDTH=100%>
		
		<TR><TD COLSPAN=2 ALIGN="CENTER" ID="resultsPane">-->
<?PHP 
if (array_key_exists("level", $_POST)){
	$interval = $_POST['interval'];
	$showAll = $_POST['showAll'];
	$hiddenCodes = $_POST['hiddenCodes'];
	$level = $_POST['level'];
	if ($_POST['critical']==0)
	{
		if ($interval == "week")
			$grouping = "Week_Ending";
		if ($interval == "period")
			$grouping = "Period_Code";
	}
	else 
		$grouping = "Date_Delivered";
	
	// Get the delivery date ranges
	if ($interval == "week"){
		$startDate = $_POST['startWeek'];
		$endDate = $_POST['endWeek'];
		$where = "DeliveryDate BETWEEN DateAdd(dd,-6,CONVERT(datetime,'".$startDate."',103)) AND CONVERT(datetime,'".$endDate."',103)";
	}else{
		$startPeriod = $_POST['startPeriod'];
		$endPeriod = $_POST['endPeriod'];
		
//		$startDate = "(SELECT TOP 1 Week_Name FROM viewLast18Months WHERE Period_Seq =".$startPeriod." ORDER BY CONVERT(DATETIME,Week_Name,103) ASC)";
//		$endDate = "(SELECT TOP 1 Week_Name FROM viewLast18Months WHERE Period_Seq =".$endPeriod." ORDER BY CONVERT(DATETIME,Week_Name,103) DESC)";
		$date_range_sql = "SELECT CONVERT(VARCHAR(20), MIN(DATEADD(dd, -6, CONVERT(DATETIME,Week_Name,103))), 103) as Start_Date, CONVERT(VARCHAR(20), MAX(CONVERT(DATETIME,Week_Name,103)), 103) as End_Date FROM viewLast18Months WHERE Period_Seq IN (".$startPeriod.", ".$endPeriod.")";
		$date_range_results = odbc_exec($link, $date_range_sql);
		while (odbc_fetch_row($date_range_results))
		{
			$startDate = odbc_result($date_range_results, "Start_Date");
			$endDate = odbc_result($date_range_results, "End_Date");
		}
		$where = "DeliveryDate BETWEEN CONVERT(datetime,'".$startDate."',103) AND CONVERT(datetime,'".$endDate."',103)";
	}
	
	/** 
	 * Construct the SQL queries to retrieve the aggregation.
	 *
	 * This uses a derived table (ingeniously called derivedTable) due to the use of a subquery to get "week_commencing"
	 * as it could not be included in the group by clause and that defeated the whole process of the aggregation
	 *
	 * The column names for concepts are generated using a PHP loop (so names are COL1,COL2...COLX) rather than the concept codes
	 * found in the SOD data. The reason for this is there is a code 'IN' which is a protected word.
	 */
	$term = '';
	if ($level=="concept"){
		$term = 'Concept';
	}
	elseif ($level=="subconcept"){
		$term = 'SubConcept';
	}
	if ($term <> ''){
		//$codes_sql = "SELECT * FROM tbl_CodeTranslation";
		$codes_sql = "SELECT * FROM tbl_".$term."s WHERE ".$term."Code <> '<N/A>'";
		$codes_sql_results = odbc_exec($link, $codes_sql);
		$sql = "SELECT ".$grouping.", WBCode, Sum(Total) As Total";
		
		$index = 0;
		
		while (odbc_fetch_row($codes_sql_results)==TRUE){
			//$value = odbc_result($codes_sql_results, "SOD_ConceptCode");
			$value = odbc_result($codes_sql_results, "".$term."Code");
			$sql .= ",SUM(COL".$index.") As '".$value."'";
			$index++;
		}
		
		$sql .= " FROM (SELECT";
		if($grouping == "Week_Ending")
			$sql .= " Calendar.Week_Name AS Week_Ending,";
		elseif($grouping == "Period_Code")
			$sql .= " Calendar.Period_Code AS Period_Code,";
//			$sql .= " CONVERT(VARCHAR(20), MAX(CONVERT(DATETIME, Calendar.Week_Name, 103)), 103) AS Period_Code,";
		else 
			$sql .= " CONVERT(VARCHAR(10),data.DeliveryDate,103) As Date_Delivered,";
	    $sql .= "data.WBCode, SUM(data.Delivered) AS Total";
	    
	    $headers = $grouping.",WBCode,Total";
	    odbc_fetch_row ($codes_sql_results ,0);
	    $index=0;
	    while (odbc_fetch_row($codes_sql_results)==TRUE){
			//$val = odbc_result($codes_sql_results, "SOD_ConceptCode");
			$val = odbc_result($codes_sql_results, "".$term."Code");
			//$sql .= ",SUM(CASE WHEN lup.[SOD_ConceptCode] = '".$val."' THEN Delivered ELSE 0 END) AS 'COL".$index."'";
			$sql .= ",SUM(CASE WHEN con.[".$term."Code] = '".$val."' THEN Delivered ELSE 0 END) AS 'COL".$index."'";
			$headers .= ",".$val;
			$index++;
		}
		
		//$sql .= " FROM dbo.tbl_Concepts AS con INNER JOIN";
		//$sql .= " dbo.tbl_CodeTranslation AS lup ON lup.ConceptCode = con.ConceptCode LEFT OUTER JOIN";
		//$sql .= " dbo.tbl_SOD_Data AS data ON lup.SOD_ConceptCode = data.Concept";
		
		if ($level=="subconcept"){
			$sql .= " FROM dbo.viewSubconcepts AS con";
			$sql .= " INNER JOIN dbo.tbl_SOD_Data AS data ON con.OutletCode = data.Outlet";
		}else{
			$sql .= " FROM dbo.tbl_".$term."s AS con INNER JOIN";
			$sql .= " dbo.tbl_SOD_Data AS data ON con.".$term."Code = data.".$term."";
		}
		$sql .= " INNER JOIN dbo.Calendar on data.DeliveryDate BETWEEN DATEADD(dd, -6, CONVERT(datetime, Calendar.Week_Name, 103)) AND CONVERT(datetime, Calendar.Week_Name, 103) ";
		
		$sql .= " WHERE (data.WBCode IS NOT NULL) AND (data.DeliveryDate IS NOT NULL) AND ";
	}else{
		//$codes_sql = "SELECT distinct BrandCode FROM tbl_CodeTranslation";
		$codes_sql = "SELECT * FROM tbl_Brands WHERE BrandCode <> '<N/A>'";
		$codes_sql_results = odbc_exec($link, $codes_sql);
		
		$sql = "SELECT ".$grouping.", WBCode,Sum(Total) As Total";
		
		while (odbc_fetch_row($codes_sql_results)==TRUE){
			$value = odbc_result($codes_sql_results, "BrandCode");
			$sql .= ",SUM(".$value.") As ".$value;
		}
		
		$sql .= " FROM (SELECT";
	    if ($grouping == "Week_Ending")
	    	$sql .= " Calendar.Week_Name AS Week_Ending,";
		elseif($grouping == "Period_Code")
			$sql .= " Calendar.Period_Code,";
//			$sql .= " CONVERT(VARCHAR(20), MAX(CONVERT(DATETIME, Calendar.Week_Name, 103)), 103) AS Period_Code,";
	    else 
	    	$sql .= " CONVERT(VARCHAR(10),data.DeliveryDate,103) As Date_Delivered,";
	    $sql .= "data.WBCode, SUM(data.Delivered) AS Total";
	    
	    $headers = $grouping.",WBCode,Total"; // column headers for the CSV file
	    odbc_fetch_row ($codes_sql_results ,0); // returns the pointer to the beginning of the array
		
	    while (odbc_fetch_row($codes_sql_results)==TRUE){
			$val = odbc_result($codes_sql_results, "BrandCode");
			$sql .= ",SUM(CASE WHEN brd.[BrandCode] = '".$val."' THEN Delivered ELSE 0 END) AS '".$val."'";
			$headers .= ",".$val;
		}
		
		$sql .= " FROM dbo.tbl_Brands AS brd INNER JOIN";
		//$sql .= " dbo.tbl_CodeTranslation AS lup ON lup.BrandCode = brd.BrandCode LEFT OUTER JOIN";
		//$sql .= " dbo.tbl_SOD_Data AS data ON lup.SOD_ConceptCode = data.Concept";
		$sql .= " dbo.tbl_Concepts as con ON con.BrandCode = brd.BrandCode INNER JOIN";
		$sql .= " dbo.tbl_SOD_Data AS data ON con.ConceptCode = data.Concept";
		$sql .= " INNER JOIN dbo.Calendar on data.DeliveryDate BETWEEN DATEADD(dd, -6, CONVERT(datetime, Calendar.Week_Name, 103)) AND CONVERT(datetime, Calendar.Week_Name, 103) ";
		$sql .= " WHERE (data.WBCode IS NOT NULL) AND (data.DeliveryDate IS NOT NULL) AND ";
	}
	
	if ($showAll != 1 && $hiddenCodes !="") {
		// add the checked product codes to where clause
		$i = 0;
		$where .= " AND (data.WBCode IN (";
		while ($i < sizeof($p_codes)-1){
			$val = $p_codes[$i];
			$where .= "'".$val."',";
			$i++;
		}
		$val = $p_codes[$i];
		$where .= "'".$val."'))";
	}
	
	$sql .= $where;
	$sql .= " GROUP BY ";
    if ($grouping == "Week_Ending") 
    	$sql .= " Calendar.Week_Name,";
	elseif($grouping == "Period_Code")
		$sql .= " Calendar.Period_Code,";
	else
		$sql .= " data.DeliveryDate,";
	$sql .= " data.WBCode) derivedTable";
	$sql .= " GROUP BY ".$grouping.", WBCode";
	$sql .= " ORDER BY CONVERT(datetime,".$grouping.",103), WBCode";
echo '<!--', $sql, '-->';
die();
	$result = odbc_exec($link, $sql);
	odbc_result_all($result, 'id="Search_Results" name="Search_Results"');	
	
	odbc_fetch_row ($result ,0);
	$result = odbc_exec($link, $sql);
	$_SESSION['headers'] = $headers;
	$_SESSION['query'] = $sql;
	$numRows = odbc_num_rows($result);
	/*$csv_data = $headers . " \n"; // write the headers to a variable that will exported to the csv file
	
	for($i=0; $i<$numRows; $i++){
		$csv_data .= implode(odbc_fetch_array($result),",") . " \n"; // created comma-delimited rows for each of the result rows
	}
	
	$_SESSION['csv_data'] = $csv_data; // Save the CSV information to a session variable so it can be accessed in download.php*/
	if (($isIE==TRUE)&& ($numRows >0)) echo " <script language=\"javaScript\" type=\"text/javascript\">resHeight();</script>";
	
	
}
?>		
		</center>
	</DIV>
	
	<DIV ID="bottomDiv"<?PHP if (!array_key_exists("level", $_POST))echo " style=display:none"?>>
		<TABLE WIDTH=100% HEIGHT=100%>
		<TR CLASS="controls">
		<TD ALIGN="CENTER"><!--<INPUT TYPE="checkbox" ID="autostart" name="autostart"><label for="autostart">Autostart Excel</label>&nbsp-->
		<INPUT TYPE="button" VALUE="Export Data" ONCLICK="window.location='download.php'">
		</TD>
		<TD ALIGN="CENTER"><INPUT TYPE="button" value="Clear Results" onClick="clearResults()"></TD></TR>
		<!-- <INPUT TYPE="button" value="Exit"> -->
		</TABLE>
	</DIV>
</DIV>
</BODY>
</HTML>
