/*var productCodes = [];


//Search the elements of a multi dimensional array - use ALPHABETICAL order (i.e. 123 comes after 1000)
//v = value to find
//d = element of array children to test against
//i = return the index that the value would go in if it isn't already there
Array.prototype.search2d_txt = function(v, d, i)
{
	var h = this.length, l = -1, m;
	while(h - l > 1)
	{
		if(''+this[m = h + l >> 1][d] < ''+v)
			l = m;
		else h = m;
	}
	return this[h][d] != v ? i ? h : -1 : h;
};

//+ Carlos R. L. Rodrigues
//@ http://jsfromhell.com/array/search [rev. #2]

//Search the elements of an array
//v = value to find
//i = return the index that the value would go in if it isn't already there
Array.prototype.search = function(v, i)
{
	var h = this.length, l = -1, m;
	while(h - l > 1)
	{
		if(this[m = h + l >> 1] < v)
			l = m;
		else h = m;
	}
	return this[h][i] != v ? i ? h : -1 : h;
};
*/
function prepare(){
	disableIntervals();
}

/*function fillProdCodes(code, description){
	var newIndex = productCodes.length;
	productCodes[newIndex] = new Array(code,description);
	
}*/

function get(item){
	return document.getElementById(item);
}

function disableIntervals(){
	var intervals = document.getElementsByName("interval");
	if (intervals[0].checked){
		//get("startDate").disabled=false;
		//get("endDate").disabled=false;
		get("startWeek").disabled=false;
		get("endWeek").disabled=false;
		get("startPeriod").disabled=true;
		get("endPeriod").disabled=true;
	}else{
		get("startWeek").disabled=true;
		get("endWeek").disabled=true;
		//get("startDate").disabled=true;
		//get("endDate").disabled=true;
		get("startPeriod").disabled=false;
		get("endPeriod").disabled=false;
	}
	selectAll();
}

function remCodes(){
	var codeList = get('codeList');
	var selIndex = codeList.selectedIndex;
	
	while (selIndex != -1){
		var code = get('codeList').value;
		var remText = codeList.options[selIndex].text;
		codeList.remove(selIndex);
		
		var codeString = get('hiddenCodes').value;
		codeString = codeString.split(",");
		var removed = codeString.splice(selIndex,1);
		get('hiddenCodes').value = codeString;
		
		var descString = get('hiddenOptions').value;
		descString = descString.split(",");
		removed = descString.splice(selIndex,1);
		get('hiddenOptions').value = descString;
		
		selIndex = codeList.selectedIndex;
	}
}

/*function addCodes(code){
	var codeString = get('hiddenCodes');
	var codeList = get('codeList');
	
	if (code!="")
	{
		if ((','+codeString.value+',').match(','+code+',') != (','+code+','))
		{
			var code_index = productCodes.search2d_txt(code, 0);
			if(code_index > -1)
			{
				var desc = "" + productCodes[code_index][0] + " (" + productCodes[code_index][1] + ")";
				codeList.options[codeList.options.length] = new Option(desc, code);
				if (codeList.length==1)
					codeString.value = code;
				else 
					codeString.value += "," + code;
				//get('code').value = "";
				return true;
			}
			alert("This code was not found in the database, please enter a valid code");
			return false;
		}
		alert("This code is already present");
		return false;
	}
	return false;
}
*/
function checkRecords(){
	var arr = document.getElementsByName('delRecords');
	if (arr[0].checked) return "all";
	else return "none";
}

function selectAll(){
	var selAll = get('showAll').checked;
	if (selAll){
		//get('code').disabled = true;
		get('addCode').disabled = true;
		get('remCode').disabled = true;
		get('codeList').disabled = true;
		//get('codeList').innerHTML="";
		get('hiddenCodes').value="";
		get('hiddenOptions').value="";
		get('codeList').options.length=0;
	}else{
		//get('code').disabled = false;
		get('addCode').disabled = false;
		get('remCode').disabled = false;
		get('codeList').disabled = false;
	}
}

function selectOption(pSelectListID, pOptionValue)
{
//alert(pSelectListID);
	var selectList = get(pSelectListID);
	var opt_index = 0;
	for(opt in selectList.options)
	{
//if(!confirm(selectList.options[opt_index].value))
//return false;
		if(selectList.options[opt_index].value == pOptionValue)
		{
//alert('found');
			selectList.options[opt_index].selected = 1;
			return true;
		}
		opt_index++;
	}
	return false;
}

function validate(){
	var IsValid = true;
	var intervals = document.getElementsByName("interval");
	//var OutletValue = get('OutletValue').value;
	var SelectedLevel = get('level').value;
	
	if (intervals[0].checked){
		var date1 = get('startWeek').selectedIndex;
		var date2 = get('endWeek').selectedIndex;
	}else{
		var date1 = get('startPeriod').selectedIndex;
		var date2 = get('endPeriod').selectedIndex;
	}
	
	if(date2 > date1){
		alert("Start date is after end date");
		IsValid = false;
	}
	
	/*if (SelectedLevel == "outlet" && OutletValue == ""){
		alert("Enter an Outlet Code");
		IsValid = false;
	}*/
	
	if (IsValid) document.form1.submit();
}

function checkSelect(){
	var url = window.location.toString();
	var level = get('level').value;
	//if ((level == 'outlet') && (url.match('add=')!='add='))get('levelOpt').style.display='block';
	if (level == 'outlet')get('levelOpt').style.display='block';
	else get('levelOpt').style.display='none';
}

function getOutletOpt(){
	var outletOpt = document.getElementsByName('outletOpt');
	if (outletOpt[0].checked)return outletOpt[0].value
	else return outletOpt[1].value
}

function isOutlet(){
	var level = get('level').value;
	if (level=="outlet")return "&outletOpt="+getOutletOpt();
	else return "";
}

function toggleDeletion(){
	var delDiv = get('deletionDiv');
	var delBtn = get('delToggle');
	
	if (delDiv.style.display=='block'){
		delDiv.style.display='none';
		delBtn.className=""
		delBtn.blur()
	}else{
		delDiv.style.display='block';
		delBtn.className="toggleDown"
		delBtn.blur()
	}
}

function clearResults(){
	get('resultsDiv').innerHTML='';
	get('resultsDiv').style.display='none';
	get('bottomDiv').style.display='none';
}

function doCodes(){
	var codes = get('hiddenOptions').value.split(',');
	var codeList = window.opener.document.getElementById('codeList');
	codeList.options.length = 0;
	//var codeList = window.opener.document.form1.codeList;
	
	for (var i=0; i<codes.length; i++){
		var prodCode = codes[i].split('(',1);
		//codeList.innerHTML+="<option value=\""+prodCode+"\">" + codes[i] + "</option>";
		var opt = window.opener.document.createElement("OPTION");
		codeList.options.add(opt);
		opt.innerHTML = codes[i];
		opt.value = prodCode;
		
	}
	window.opener.document.getElementById('hiddenOptions').value=get('hiddenOptions').value;
	window.opener.document.getElementById('hiddenCodes').value=get('hiddenCodes').value;
	window.close();
}

/*function popupCodes(){
	var codes = window.opener.document.getElementById('hiddenOptions').value.split(",");
	var codeList = get('codeList');
	for (var i=0; i<codes.length; i++){
		//codeList.innerHTML+="<option>" + codes[i] + "</option>";
	}
}
*/
function getExistingCodes(){
	var oldCodeList = window.opener.document.getElementById('codeList');
	var codeList = get('codeList');
	var hiddenOptions = get('hiddenOptions');
	var hiddenCodes = get('hiddenCodes');
	for (var i=0; i<oldCodeList.options.length; i++){
		var oldText = oldCodeList.options[i].text;
		var oldCode = oldCodeList.options[i].value;
		//codeList.innerHTML+="<option>"+oldCodeList.options[i].text+"</option>";
		codeList.options[i] = new Option(oldText,oldCode);
		if (i==0){
			hiddenOptions.value=oldText;
			hiddenCodes.value=oldCode;
		}else{
			hiddenOptions.value+="," + oldText;
			hiddenCodes.value+="," + oldCode;
		}
	}
}

function resHeight(){
	var tableHeight = get('Search_Results').clientHeight;
	if (tableHeight >= 210)get('resultsDiv').style.height=240;
	else get('resultsDiv').style.height=tableHeight+50;
}

function clearText(obj){
	if ((obj.value=="Enter Code") || (obj.value=="Enter Name"))obj.value="";
}

/*function toggleOutletInput(){
	var outlet = get('OutletValue');
	var levelSelected = get('level').value;
	
	outlet.value = "";
	
	if (levelSelected == 'outlet')
		outlet.style.display = 'inline';
	else
		outlet.style.display = 'none';
}*/

function toggleSupplierInput(){
	var supplier = get('SupplierName');
	var byday = get('critical');
	//var exportsupplier = get('exportsupplier');
	//var retrieve = get('retrieve');
	var levelSelected = get('level').value;
	
	supplier.value = "";
	
	if (levelSelected == 'supplier'){
		supplier.style.display = 'inline';
		//exportsupplier.style.display = 'inline';
		//retrieve.style.display = 'none';
	}else{
		supplier.style.display = 'none';
		//exportsupplier.style.display = 'none';
		//retrieve.style.display = 'inline';
	}
	
	if (levelSelected == 'costaorder'){
		byday.disabled = true;
	}else{
		byday.disabled = false;
	}
}