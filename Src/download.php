<?PHP
	include('includes/links.php');
	set_time_limit(300);
	session_start();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Content-type: application/force-download");
	if ($_SESSION['level'] != 'supplier')
		header("Content-Disposition: attachment; filename=report.csv");
	else
		header("Content-Disposition: attachment; filename=SupplierReport_".$_SESSION['Supplier'].".csv");

	$sql = $_SESSION['query'];
	$result = odbc_exec($link, $sql);

	$file = fopen('php://output', 'w');
	fwrite($file, $_SESSION['headers'] . " \n"); // write the headers 

	while ($result_array = odbc_fetch_array($result))
	{
		if ($_SESSION['level'] != 'outlet')
			fputcsv($file, $result_array);
		else
		{
			$result_array = array_values($result_array);
			unset($result_array[0]); //Remove the Week/Period Column
			fputcsv($file, $result_array);
		}
	}
	fclose($file);
?>
