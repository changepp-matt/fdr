<?PHP 
	session_start();
	include('includes/links.php');
	
	$interval = $_POST['interval'];
	$hiddenList = $_POST['hiddenList'];
	$p_codes = explode(",",$hiddenList);
	$level = $_POST['level'];
	
?>

<HTML>
<HEAD>
	<script language="javaScript" type="text/javascript" src="javascript.js"></script>
	<link href="style.css" rel="stylesheet" type="text/css" media="screen">
	<TITLE>Food Delivery Reporting For Whitbread Supply Chain</TITLE>
</HEAD>

<BODY onLoad="prepare()">
	<?PHP
	//Find all WBCode, Description combinations and echo the results into JS function fillProdCodes() to Populate JS array
	$sql = "SELECT DISTINCT WBCode,Description FROM tbl_Dishcost";
	$codes_res = odbc_exec($link, $sql);
	echo "<script language=\"Javascript\" type=\"text/javascript\">";
	
	while (odbc_fetch_row($codes_res)==TRUE){
		$code = odbc_result($codes_res, "WBCode");
		$desc = odbc_result($codes_res, "Description");
		if (!strchr($desc,'""')==FALSE){
			$desc = str_replace('""','inch',$desc);
			$desc = str_replace('"','',$desc);
		}
		echo "fillProdCodes(\"" .$code. "\" , \"" .$desc. "\");\n";
		if ($code != ""){
			if ((in_array($code,$p_codes)) && (strstr($prodCodes,$code)==FALSE)){
				$prodCodes .= "<option value=".$code.">".$code."(".$desc.")</option>\n";
				if (!isset($hiddenListCodes))$hiddenListCodes = $code;
				else $hiddenListCodes .= ",".$code;
			}
		}
	}
	echo"</script>\n";
			
	?>
	
	<DIV ID="intervalSelection">
	<!-- This table structures the page's title and link --> 
	<table width=100%>
		<tr>
			<td align=center>
				<img src="images/logo.gif" align=top alt="Whitbread logo">
				<br><br>
				<a href="manage.php">Heirarchy Management</a>
			</td>
			<td align=center>
				<h1>Food Delivery Reporting</h1>
				<h2>Reporting Page</h2>
			</td>
		</tr>
	</table>
	<p>
	<center><img src="images/divider.gif" alt="blue divider"></center><p>
	
	<FORM NAME="form1" method="POST" action="reporting.php">
	<TABLE CELLPADDING=5 WIDTH=100%>
	<!--<TR>
		<TD ALIGN="LEFT"><INPUT TYPE="Radio" NAME="interval" VALUE="day" ID="day" <?PHP if((!isset($interval)) || $interval=="day") echo "CHECKED" ?> ONCLICK="disableIntervals()"><LABEL FOR="day">Day</LABEL></TD>
		<TD ALIGN="RIGHT"><LABEL FOR="startDate">From Date: </LABEL>
			<INPUT TYPE="text" NAME="startDate" ID="startDate" MAXLENGTH=10 SIZE=10 <?PHP if ($interval=="day") echo "VALUE=".$_POST['startDate']?>>
			&nbsp<IMG SRC="images/cal.gif" ALT="Show Calendar" ONCLICK="show_calendar('startDate', form1.startDate.value)" onmouseover="this.style.cursor='pointer'"></TD>
		<TD ALIGN="RIGHT"><LABEL FOR="endDate">To Date: </LABEL>
			<INPUT TYPE="text" NAME="endDate" ID="endDate" MAXLENGTH=10 SIZE=10 <?PHP if ($interval=="day") echo "VALUE=".$_POST['endDate']?>>
			&nbsp<IMG SRC="images/cal.gif" ALT="Show Calendar" ONCLICK="show_calendar('endDate', form1.endDate.value)"></TD>
	</TR>
	-->
	<TR>
		<TD ALIGN="LEFT"><INPUT <?PHP if((!isset($interval)) || $interval=="week") echo "CHECKED" ?> TYPE="Radio" NAME="interval" VALUE="week" ID="week" ONCLICK="disableIntervals()"><LABEL FOR="week">Week</LABEL></TD>
		<TD ALIGN="RIGHT"><LABEL FOR="startWeek">From Week Commencing: </LABEL>
			<SELECT NAME="startWeek" ID="startWeek">
				<?PHP
				// Get all applicable Weeks From DB view, viewLast18Months
				$sql = "SELECT Week_Name FROM viewLast18Months ORDER BY Week_Seq DESC";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Week_Name");
					if($interval=="week" && $_POST['startWeek']==$val)echo "<OPTION SELECTED VALUE=".$val.">".$val."</OPTION>\n";
					else echo "<OPTION VALUE=".$val.">".$val."</OPTION>\n";
				}
				?>
			</SELECT>
		</TD>
		<TD ALIGN="RIGHT"><LABEL FOR="endWeek">Until END OF Week Commencing: </LABEL>
			<SELECT NAME="endWeek" ID="endWeek">
				<?PHP 
				$sql = "SELECT Week_Name FROM viewLast18Months ORDER BY Week_Seq DESC";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Week_Name");
					if($interval=="week" && $_POST['endWeek']==$val)echo "<OPTION SELECTED VALUE=".$val.">".$val."</OPTION>\n";
					else echo "<OPTION VALUE=".$val.">".$val."</OPTION>\n";
				}
				?>
			</SELECT>
		</TD>
	</TR>
	
	<TR>
		<TD ALIGN="LEFT"><INPUT <?PHP if($interval=="period") echo "CHECKED" ?> TYPE="Radio" NAME="interval" VALUE="period" ID="period" ONCLICK="disableIntervals()"><LABEL FOR="period">Period</LABEL></TD>
		<TD ALIGN="RIGHT"><LABEL FOR="startPeriod">From Period: </LABEL>
			<SELECT NAME="startPeriod" ID="startPeriod">
				<?PHP
				// Get all applicable Periods from viewLast18Months
				$sql = "SELECT DISTINCT Period_Seq FROM viewLast18Months ORDER BY Period_Seq DESC";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Period_Seq");
					if($interval=="period" && $_POST['startPeriod']==$val)echo "<OPTION SELECTED VALUE=".$val.">".$val."</OPTION>\n";
					else echo "<OPTION VALUE=".$val.">".$val."</OPTION>\n";
				}
				?>
			</SELECT>
		</TD>
		<TD ALIGN="RIGHT"><LABEL FOR="endPeriod">To End Of Period: </LABEL>
			<SELECT NAME="endPeriod" ID="endPeriod">
				<?PHP 
				$sql = "SELECT DISTINCT Period_Seq FROM viewLast18Months ORDER BY Period_Seq DESC";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$val = odbc_result($result, "Period_Seq");
					if($interval=="period" && $_POST['endPeriod']==$val)echo "<OPTION SELECTED VALUE=".$val.">".$val."</OPTION>\n";
					else echo "<OPTION VALUE=".$val.">".$val."</OPTION>\n";
				}
				?>
			</SELECT>
		</TD>
	</TR>
	<TR><TD COLSPAN=3 ALIGN="CENTER"><IMG SRC="images/divider.gif"></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=3>
		<LABEL FOR="level">Aggregation Level: </LABEL><SELECT NAME="level" ID="level">
			<OPTION <?PHP if ($level=="brand")echo "SELECTED";?> VALUE="brand">Brand</OPTION>
			<OPTION <?PHP if ($level=="concept")echo "SELECTED";?> VALUE="concept">Concept</OPTION>
			<!--<OPTION <?PHP if ($level=="outlet")echo "SELECTED";?> VALUE="outlet">Outlet</OPTION>-->
		</SELECT></TD></TR>
	<TR><TD COLSPAN=3 ALIGN="CENTER"><IMG SRC="images/divider.gif"></TD></TR>
	<TR><TD ALIGN="CENTER">
		<LABEL for="showAll">Show All Products</LABEL><INPUT <?PHP if (sizeof($prodCodes)==0) echo "CHECKED";?> TYPE="checkbox" VALUE=1 ID="showAll" name="showAll" onClick="selectAll()">
		
		<p><input type="text" id="code" name="code" value="">
		<input type="button" id="addCode" name="addCode" value="Add Code" onclick="addCodes()">
		
		<p><input type="button" id="remCode" name="remCode" value="Remove Code" onclick="remCodes()">
		
	</td><td colspan=2>
		<select size=5 id="codeList" name="codeList" style="width:300px">
		<?PHP
			if (isset($p_codes))echo $prodCodes;
		?>
		</select>
		<input type="hidden" id="hiddenList" name="hiddenList" value="<?PHP echo $hiddenListCodes ?>">
	</TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=3><INPUT TYPE="submit" value="Preview"></TD></TR>
	</TABLE>
	</FORM>
	</DIV>
	
	<DIV ID="resultsDiv">
		<TABLE ALIGN="CENTER" HEIGHT=100% WIDTH=100%>
		
		<TR><TD COLSPAN=2 ALIGN="CENTER" ID="resultsPane">
<?PHP 
if (array_key_exists("level", $_POST)){
	$interval = $_POST['interval'];
	$showAll = $_POST['showAll'];
	$hiddenList = $_POST['hiddenList'];
	$level = $_POST['level'];
	
	// Get the delivery date ranges
	if ($interval == "week"){
		$startDate = $_POST['startWeek'];
		$endDate = $_POST['endWeek'];
		$where = "DeliveryDate BETWEEN CONVERT(datetime,'".$startDate."',103) AND DATEADD(dd,6,CONVERT(datetime,'".$endDate."',103))";
	}else{
		$startPeriod = $_POST['startPeriod'];
		$endPeriod = $_POST['endPeriod'];
		
		$startDate = "(SELECT TOP 1 Week_Name FROM viewLast18Months WHERE Period_Seq =".$startPeriod." ORDER BY Week_Name ASC)";
		$endDate = "(SELECT TOP 1 Week_Name FROM viewLast18Months WHERE Period_Seq =".$endPeriod." ORDER BY Week_Name DESC)";
		$where = "DeliveryDate BETWEEN CONVERT(datetime,".$startDate.",103) AND CONVERT(datetime,".$endDate.",103)";
	}
	
	/** 
	 * Construct the SQL queries to retrieve the aggregation.
	 *
	 * This uses a derived table (ingeniously called derivedTable) due to the use of a subquery to get "week_commencing"
	 * as it could not be included in the group by clause and that defeated the whole process of the aggregation
	 *
	 * The column names for concepts are generated using a PHP loop (so names are COL1,COL2...COLX) rather than the concept codes
	 * found in the SOD data. The reason for this is there is a code 'IN' which is a protected word.
	 */
	if ($level=="concept"){
		//$codes_sql = "SELECT * FROM tbl_CodeTranslation";
		$codes_sql = "SELECT * FROM tbl_SubConcepts";
		$codes_sql_results = odbc_exec($link, $codes_sql);
		
		$sql = "SELECT Week_Commencing, WBCode,Sum(Total) As Total";
		
		$index = 0;
		
		while (odbc_fetch_row($codes_sql_results)==TRUE){
			//$value = odbc_result($codes_sql_results, "SOD_ConceptCode");
			$value = odbc_result($codes_sql_results, "SubConceptCode");
			$sql .= ",SUM(COL".$index.") As '".$value."'";
			$index++;
		}
		
		$sql .= " FROM (SELECT";
		$sql .= " (SELECT TOP (1) Week_Name FROM dbo.Calendar WHERE (CONVERT(datetime, Week_Name, 103) <= data.DeliveryDate) ORDER BY Week_Seq DESC) AS Week_Commencing,";
	    $sql .= "data.WBCode, SUM(data.Delivered) AS Total";
	    
	    $headers = "Week_Commencing,WBCode,Total";
	    odbc_fetch_row ($codes_sql_results ,0);
	    $index=0;
	    while (odbc_fetch_row($codes_sql_results)==TRUE){
			//$val = odbc_result($codes_sql_results, "SOD_ConceptCode");
			$val = odbc_result($codes_sql_results, "SubConceptCode");
			//$sql .= ",SUM(CASE WHEN lup.[SOD_ConceptCode] = '".$val."' THEN Delivered ELSE 0 END) AS 'COL".$index."'";
			$sql .= ",SUM(CASE WHEN con.[SubConceptCode] = '".$val."' THEN Delivered ELSE 0 END) AS 'COL".$index."'";
			$headers .= ",".$val;
			$index++;
		}
		
		//$sql .= " FROM dbo.tbl_Concepts AS con INNER JOIN";
		//$sql .= " dbo.tbl_CodeTranslation AS lup ON lup.ConceptCode = con.ConceptCode LEFT OUTER JOIN";
		//$sql .= " dbo.tbl_SOD_Data AS data ON lup.SOD_ConceptCode = data.Concept";
		$sql .= " FROM dbo.tbl_SubConcepts AS con INNER JOIN";
		$sql .= " dbo.tbl_SOD_Data AS data ON con.SubConceptCode = data.SubConcept";
		$sql .= " WHERE (data.WBCode IS NOT NULL) AND (data.DeliveryDate IS NOT NULL) AND ";
	}else{
		//$codes_sql = "SELECT distinct BrandCode FROM tbl_CodeTranslation";
		$codes_sql = "SELECT * FROM tbl_Brands WHERE BrandCode <> '<N/A>'";
		$codes_sql_results = odbc_exec($link, $codes_sql);
		
		$sql = "SELECT Week_Commencing, WBCode,Sum(Total) As Total";
		
		while (odbc_fetch_row($codes_sql_results)==TRUE){
			$value = odbc_result($codes_sql_results, "BrandCode");
			$sql .= ",SUM(".$value.") As ".$value;
		}
		
		$sql .= " FROM (SELECT";
	    $sql .= " (SELECT TOP (1) Week_Name FROM dbo.Calendar WHERE (CONVERT(datetime, Week_Name, 103) <= data.DeliveryDate) ORDER BY Week_Seq DESC) AS Week_Commencing,";
	    $sql .= "data.WBCode, SUM(data.Delivered) AS Total";
	    
	    $headers = "Week_Commencing,WBCode,Total"; // column headers for the CSV file
	    odbc_fetch_row ($codes_sql_results ,0); // returns the pointer to the beginning of the array
		
	    while (odbc_fetch_row($codes_sql_results)==TRUE){
			$val = odbc_result($codes_sql_results, "BrandCode");
			$sql .= ",SUM(CASE WHEN brd.[BrandCode] = '".$val."' THEN Delivered ELSE 0 END) AS '".$val."'";
			$headers .= ",".$val;
		}
		
		$sql .= " FROM dbo.tbl_Brands AS brd INNER JOIN";
		//$sql .= " dbo.tbl_CodeTranslation AS lup ON lup.BrandCode = brd.BrandCode LEFT OUTER JOIN";
		//$sql .= " dbo.tbl_SOD_Data AS data ON lup.SOD_ConceptCode = data.Concept";
		$sql .= " dbo.tbl_Concepts as con ON con.BrandCode = brd.BrandCode INNER JOIN";
		$sql .= " dbo.tbl_SOD_Data AS data ON con.ConceptCode = data.Concept";
		$sql .= " WHERE (data.WBCode IS NOT NULL) AND (data.DeliveryDate IS NOT NULL) AND ";
	}
	
	if ($showAll != 1 && $hiddenList !="") {
		// add the checked product codes to where clause
		$i = 0;
		$where .= " AND (";
		while ($i < sizeof($p_codes)-1){
			$val = $p_codes[$i];
			$where .= "data.WBCode=".$val." OR ";
			$i++;
		}
		$val = $p_codes[$i];
		$where .= "data.WBCode=".$val.")";
	}
	
	$sql .= $where;
	$sql .= " GROUP BY data.DeliveryDate, data.WBCode) derivedTable";
	$sql .= " GROUP BY Week_Commencing, WBCode";
	$sql .= " ORDER BY CONVERT(datetime,Week_Commencing,103), WBCode";
	//echo $sql;
	$result = odbc_exec($link, $sql);
	odbc_result_all($result, 'id="Search_Results" name="Search_Results"');	
	
	odbc_fetch_row ($result ,0);
	$result = odbc_exec($link, $sql);
	$csv_data = $headers . " \n"; // write the headers to a variable that will exported to the csv file
	$numRows = odbc_num_rows($result);
	
	for($i=0; $i<$numRows; $i++){
		$csv_data .= implode(odbc_fetch_array($result),",") . " \n"; // created comma-delimited rows for each of the result rows
	}
	
	$_SESSION['csv_data'] = $csv_data; // Save the CSV information to a session variable so it can be accessed in download.php
	
}
?>		
		</TD></TR>
		</TABLE>
	</DIV>
	
	<DIV ID="bottomDiv">
		<TABLE WIDTH=100% HEIGHT=100%>
		<TR CLASS="controls">
		<TD ALIGN="CENTER"><!--<INPUT TYPE="checkbox" ID="autostart" name="autostart"><label for="autostart">Autostart Excel</label>&nbsp-->
		<INPUT TYPE="button" VALUE="Export Data" ONCLICK="window.location='download.php'">
		</TD>
		<TD ALIGN="CENTER"><INPUT TYPE="button" value="Clear Results" onClick="get('resultsPane').innerHTML=''"></TD></TR>
		<!-- <INPUT TYPE="button" value="Exit"> -->
		</TABLE>
</BODY>
</HTML>