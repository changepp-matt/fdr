<?PHP
	set_time_limit(60);
	include('includes/links.php');
	$level = $_GET['level'];
	$selection = $_GET['selection'];
		
	$newName = $_POST['name'];
	$newCode = $_POST['code'];
	$currCode = $_GET['save'];
	$bottomLevel = '</form>';
	if (array_key_exists("unassigned",$_GET)){
		$sql = "SELECT * FROM viewUnassigned";
		$result = odbc_exec($link,$sql);
		if (odbc_num_rows($result)==0)$bottomLevel = '</form><p><img src="images/divider.gif" align=center alt="blue divider"><p>There are no new Outlet Codes';
		else{
			$bottomLevel = '</form><p><img src="images/divider.gif" align=center alt="blue divider"><p>';
			$bottomLevel .= '<form name="new_outlets" id="new_outlets" action="?add=outlet" method="POST">';
			$bottomLevel .= '<input type="hidden" id="outletCode" name="outletCode"><TABLE><TR><TD><b>Click a code to add to the hierarchy</B></TD></TR>';
			while(odbc_fetch_row($result)==TRUE){
				$bottomLevel .= '<tr><td align="center">';
				$bottomLevel .= '<a onmouseover="this.style.cursor=\'pointer\';this.style.color=\'#FF0000\'" onmouseout="this.style.color=\'#000000\'" onClick="new_outlets.outletCode.value=this.innerHTML;new_outlets.submit()">'.odbc_result($result,"Outlet").'</a></td></tr>';
				
			}
			$bottomLevel .= '</TABLE></form>';
		}
	}else if (array_key_exists("save",$_GET)){
		// Update existing items
		if ($level=="brand"){
			$sql = "UPDATE tbl_hierarchy SET BrandName='".$newName."', BrandCode='".$newCode."' WHERE BrandCode='".$currCode."' ";
			$sql .= "UPDATE tbl_Brands SET BrandName='".$newName."', BrandCode='".$newCode."' WHERE BrandCode='".$currCode."'";
			$result = odbc_exec($link, $sql);
			if ($result!==FALSE)$bottomLevel = "The Brand ".$newName." has been updated";
			else $bottomLevel = "The update of Brand ".$newName." Failed";
		}else if ($level=="concept"){
			$newBrandCode = $_POST['brand'];
			$sql = "SELECT BrandName FROM tbl_Brands WHERE BrandCode='".$newBrandCode."'";
			$result = odbc_exec($link, $sql);
			$newBrandName = odbc_result($result,"BrandName");
			$sql = "UPDATE tbl_hierarchy SET BrandName='".$newBrandName."',BrandCode='".$newBrandCode."', ConceptName='".$newName."', ConceptCode='".$newCode."' WHERE ConceptCode='".$currCode."'";
			$sql .= "UPDATE tbl_Concepts SET BrandCode='".$newBrandCode."', ConceptName='".$newName."', ConceptCode='".$newCode."' WHERE ConceptCode='".$currCode."'";
			$result = odbc_exec($link, $sql);			
			if ($result!==FALSE)$bottomLevel = "The Concept ".$newName." has been updated";
			else $bottomLevel = "The update of Concept ".$newName." Failed";
		}else if ($level=="subconcept"){
			$newConceptCode = $_POST['concept'];
			$sql = "SELECT ConceptName FROM tbl_Concepts WHERE ConceptCode='".$newConceptCode."'";
			$result = odbc_exec($link, $sql);
			$newConceptName = odbc_result($result,"ConceptName");
			$sql = "UPDATE tbl_hierarchy SET ConceptName='".$newConceptName."', ConceptCode='".$newConceptCode."', SubConceptName='".$newName."', SubConceptCode='".$newCode."' WHERE SubConceptCode='".$currCode."' ";
			$sql .= "UPDATE tbl_SubConcepts SET SubConceptCode='".$newCode."', SubConceptName='".$newName."' WHERE SubConceptCode='".$currCode."'";
			$result = odbc_exec($link, $sql);
			if ($result!==FALSE)$bottomLevel = "The Sub-Concept ".$newName." has been updated";
			else $bottomLevel = "The update of Sub-Concept ".$newName." Failed";
		}else if ($level=="outlet"){
			$newSubConceptCode = $_POST['subconcept'];
			$sql = "SELECT SubConceptName FROM tbl_SubConcepts WHERE SubConceptCode='".$newSubConceptCode."'";
			$result = odbc_exec($link, $sql);
			$newSubConceptName = odbc_result($result,"SubConceptName");
			$sql = "UPDATE tbl_hierarchy SET SubConceptCode='".$newSubConceptCode."', SubConceptName='".$newSubConceptName."', OutletName='".$newName."', OutletCode='".$newCode."' WHERE OutletCode='".$currCode."'";
			$result = odbc_exec($link, $sql);
			if ($result!==FALSE)$bottomLevel = "The Outlet ".$newName." has been updated";
			else $bottomLevel = "The update of Outlet ".$newName." Failed";
		}
	}else if(array_key_exists("create",$_GET)){
		// Add new item to database
		$name = $_POST['name'];
		$code = $_POST['code'];
		$level = $_GET['create'];
		
		if ($level=="brand"){
			$check_sql = "SELECT COUNT(*) as 'numRows' FROM tbl_Brands WHERE BrandCode='".$code."'";
			$result = odbc_exec($link, $check_sql);
			$count = odbc_result($result,"numRows");
			if ($count == 0){
				$sql = "INSERT INTO tbl_Brands VALUES('".$code."','".$name."')";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Brand ".$name." has been created";
				else $bottomLevel = "The creation of Brand ".$name." Failed";
			}else{
				$bottomLevel = "This Brand Code already exists!";
			}
		}else if ($level=="concept"){
			$BrandCode = $_POST['brand'];
			$sql = "SELECT BrandName FROM tbl_Brands WHERE BrandCode='".$BrandCode."'";
			$result = odbc_exec($link, $sql);
			$BrandName = odbc_result($result,"BrandName");
			
			$check_sql = "SELECT COUNT(*) as 'numRows' FROM tbl_Concepts WHERE ConceptCode='".$code."'";
			$result = odbc_exec($link, $check_sql);
			$count = odbc_result($result,"numRows");
			if ($count == 0){
				$sql = "INSERT INTO tbl_Concepts VALUES ('".$code."','".$name."','".$BrandCode."')";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Concept ".$name." has been created";
				else $bottomLevel = "The creation of Concept ".$name." Failed";
			}else{
				$bottomLevel = "This Concept Code already exists!";
			}
		}else if ($level=="subconcept"){
			$ConceptCode = $_POST['concept'];
			$sql = "SELECT ConceptName FROM tbl_Concepts WHERE ConceptCode='".$ConceptCode."'";
			$result = odbc_exec($link, $sql);
			$ConceptName = odbc_result($result,"ConceptName");
			
			$check_sql = "SELECT COUNT(*) as 'numRows' FROM tbl_Hierarchy WHERE OutletCode='".$code."'";
			$result = odbc_exec($link, $check_sql);
			$count = odbc_result($result,"numRows");
			if ($count == 0){
				$sql = "INSERT INTO tbl_SubConcepts VALUES ('".$code."','".$name."','".$ConceptCode."')";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Sub-Concept ".$name." has been created";
				else $bottomLevel = "The creation of Sub-Concept ".$name." Failed";
			}else{
				$bottomLevel = "This Outlet Code already exists!";
			}
		}else if ($level=="outlet"){
			$SubConceptCode = $_POST['subconcept'];
			$sql = "SELECT b.BrandCode,b.BrandName,c.ConceptName, c.ConceptCode, s.SubConceptName FROM tbl_Concepts as c INNER JOIN tbl_Brands as b ON c.BrandCode = b.BrandCode INNER JOIN tbl_SubConcepts as s ON s.ConceptCode = c.ConceptCode WHERE s.SubConceptCode='".$SubConceptCode."'";
			$result = odbc_exec($link, $sql);
			$BrandCode = odbc_result($result,"BrandCode");
			$BrandName = odbc_result($result,"BrandName");
			$ConceptCode = odbc_result($result,"ConceptCode");
			$ConceptName = odbc_result($result,"ConceptName");
			$SubConceptName = odbc_result($result,"SubConceptName");
			
			$check_sql = "SELECT COUNT(*) as 'numRows' FROM tbl_Hierarchy WHERE OutletCode='".$code."'";
			$result = odbc_exec($link, $check_sql);
			$count = odbc_result($result,"numRows");
			if ($count == 0){
				$sql = "INSERT INTO tbl_Hierarchy VALUES ('".$BrandCode."','".$BrandName."','".$ConceptCode."','".$ConceptName."','".$SubConceptCode."','".$SubConceptName."','".$code."','".$name."', NULL)";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Outlet ".$name." has been created";
				else $bottomLevel = "The creation of Outlet ".$name." Failed";
			}else{
				$bottomLevel = "This Outlet Code already exists!";
			}
		}
	}else if (array_key_exists("add",$_GET)){
		// Display the options for adding a new item
		$level = $_GET['add'];
		$bottomLevel = '</form><p><img src="images/divider.gif" align=center alt="blue divider"><p>';
		$bottomLevel .= '<FORM NAME="details" method="POST" action="?create='.$level.'">';
		if ($level=="brand"){
			$bottomLevel .= '<table><tr><td align=left>';
			$bottomLevel .= '<label for="name">Brand Name:</label>';
			$bottomLevel .= '</td><td  align=left>';
			$bottomLevel .= '<input type=text name="name" id="name" value="Enter Name" onClick="clearText(this)">';
			$bottomLevel .= '</td></tr><tr><td  align=left>';
			$bottomLevel .= '<label for="code">Brand Code:</label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<input type=text name="code" id="code" value="Enter Code" onClick="clearText(this)">';
			$bottomLevel .= '</td></tr></table>';
			
			$bottomLevel .= '<p>';
			$bottomLevel .= '<input type="submit" value="Create Brand">';
		}else if ($level=="concept"){
			$bottomLevel .= '<table><tr><td align=left>';
			$bottomLevel .= '<label for="name">Concept Name:</label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<input type=text name="name" id="name" value="Enter Name" onClick="clearText(this)"><br>';
			$bottomLevel .= '</td></tr><tr><td align=left>';
			$bottomLevel .= '<label for="code">Concept Code:</label>';
			$bottomLevel .= '</td><td>';
			$bottomLevel .= '<input type=text name="code" id="code" value="Enter Code" onClick="clearText(this)"><br>';
			$bottomLevel .= '</td></tr><tr><td  align=left>';
			$bottomLevel .= '<label for="brand">Brand:</label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<select name="brand" id="brand">';
			
			$sql = "SELECT * FROM tbl_Brands ORDER BY BrandCode";
			$result = odbc_exec($link, $sql);
			while (odbc_fetch_row($result)==TRUE){
				$text = odbc_result($result, "BrandName");
				$val = odbc_result($result, "BrandCode");
				$bottomLevel.= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
			}
			$bottomLevel .= '</select><br>';
			$bottomLevel .= '</td></tr></table>';
			
			$bottomLevel .= '<p>';
			$bottomLevel .= '<input type="submit" value="Create Concept">';
			
		}else if ($level=="subconcept"){
			$bottomLevel .= '<table><tr><td align=left>';
			$bottomLevel .= '<label for="name">Sub-Concept Name: </label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<input type=text name="name" id="name" value="Enter Name" onClick="clearText(this)">';
			$bottomLevel .= '</td></tr><tr><td  align=left>';
			$bottomLevel .= '<label for="code">Sub-Concept Code: </label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<input type=text name="code" id="code" value="Enter Code" onClick="clearText(this)">';
			$bottomLevel .= '</td></tr><tr><td  align=left>';
			$bottomLevel .= '<label for="concept">Concept: </label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<select name="concept" id="concept">';
			
			$sql = "SELECT * FROM tbl_Concepts ORDER BY ConceptCode";
			$result = odbc_exec($link, $sql);
			while (odbc_fetch_row($result)==TRUE){
				$text = odbc_result($result, "ConceptName");
				$val = odbc_result($result, "ConceptCode");
				if ($val==$currConcept)$bottomLevel.= '<OPTION SELECTED VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
				else $bottomLevel.= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
			}
			$bottomLevel .= '</select><br>';
			$bottomLevel .= '</td></tr></table>';
			
			$bottomLevel .= '<p>';
			$bottomLevel .= '<input type="submit" value="Create Sub-Concept">';
		}else if ($level=="outlet"){
			if ($_POST['outletCode']==''){
				$outletCodeValue="Enter Code";
				$readonly = " ";
			}
			else{
				$outletCodeValue=$_POST['outletCode'];
				$readonly = ' READONLY style="background-color:#CCCCCC"';
			}
			$bottomLevel .= '<table><tr><td align=left>';
			$bottomLevel .= '<label for="name">Outlet Name: </label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<input type=text name="name" id="name" value="Enter Name" onClick="clearText(this)">';
			$bottomLevel .= '</td></tr><tr><td align=left>';
			$bottomLevel .= '<label for="code">Outlet Code: </label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<input'.$readonly.'type=text name="code" id="code" value="'.$outletCodeValue.'" onClick="clearText(this)">';
			$bottomLevel .= '</td></tr><tr><td align=left>';
			$bottomLevel .= '<label for="concept">Sub-Concept: </label>';
			$bottomLevel .= '</td><td align=left>';
			$bottomLevel .= '<select name="subconcept" id="subconcept">';
			
			$sql = "SELECT * FROM tbl_SubConcepts ORDER BY SubConceptCode";
			$result = odbc_exec($link, $sql);
			while (odbc_fetch_row($result)==TRUE){
				$text = odbc_result($result, "SubConceptName");
				$val = odbc_result($result, "SubConceptCode");
				if ($val==$currConcept)$bottomLevel.= '<OPTION SELECTED VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
				else $bottomLevel.= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
			}
			$bottomLevel .= '</select><br>';
			$bottomLevel .= '</td></tr></table>';

			$bottomLevel .= '<p>';
			$bottomLevel .= '<input type="submit" value="Create Outlet">';
		}
		$bottomLevel .= '</form>';
	}else if (array_key_exists("delete",$_GET)){
		$level = $_GET['level'];
		$item = $_GET['delete'];
		$delRecords = $_GET['delRecords'];
		/** 
		 * if $delRecords == "all" then remove ALL records in the database that refer to the relevant item
		 * otherwise simply mark them as "UNALLOCATED"
		 */
		
		if ($level == "brand"){
			if ($delRecords == "all"){
				$sql = "DELETE FROM tbl_hierarchy WHERE BrandCode='".$item."' ";
				$sql .= "DELETE FROM tbl_Brands WHERE BrandCode='".$item."' ";
				$sql .= "DELETE FROM tbl_Concepts WHERE BrandCode='".$item."'";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Brand with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the brand with code ".$item." failed";
			}else{
				$sql = "UPDATE tbl_hierarchy SET BrandCode='<N/A>', BrandName='UNALLOCATED' WHERE BrandCode='".$item."' ";
				$sql .= "UPDATE tbl_Concepts SET BrandCode='<N/A>' WHERE BrandCode='".$item."'";
				$sql .= "DELETE FROM tbl_Brands WHERE BrandCode='".$item."' ";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Brand with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the brand with code ".$item." failed";
			}
		}else if ($level == "concept"){
			if ($delRecords == "all"){
				$sql = "DELETE FROM tbl_hierarchy WHERE ConceptCode='".$item."' ";
				$sql .= "DELETE FROM tbl_Concepts WHERE ConceptCode='".$item."'";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Concept with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the concept with code ".$item." failed";
			}else{
				$sql = "UPDATE tbl_hierarchy SET ConceptCode='<N/A>', ConceptName='UNALLOCATED' WHERE ConceptCode='".$item."' ";
				$sql .= "DELETE FROM tbl_Concepts WHERE ConceptCode='".$item."'";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Concept with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the concept with code ".$item." failed";
			}
		}else if ($level == "subconcept"){
			if ($delRecords == "all"){
				$sql = "DELETE FROM tbl_hierarchy WHERE SubConceptCode='".$item."' ";
				$sql .= "DELETE FROM tbl_SubConcepts WHERE SubConceptCode='".$item."'";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Sub-Concept with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the Sub-Concept with code ".$item." failed";
			}else{
				$sql = "UPDATE tbl_hierarchy SET SubConceptCode='<N/A>', SubConceptName='UNALLOCATED' WHERE SubConceptCode='".$item."' ";
				$sql .= "DELETE FROM tbl_SubConcepts WHERE SubConceptCode='".$item."'";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The Sub-Concept with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the Sub-Concept with code ".$item." failed";
			}
		}else if ($level == "outlet"){
			if ($delRecords == "all"){
				$sql = "DELETE FROM tbl_hierarchy WHERE OutletCode='".$item."' ";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The outlet with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the outlet with code ".$item." failed";
			}else{
				$sql = "UPDATE tbl_hierarchy SET OutletCode=NULL, OutletName=NULL WHERE OutletCode='".$item."' ";
				$result = odbc_exec($link, $sql);
				if ($result!==FALSE)$bottomLevel = "The outlet with code ".$item." has been deleted";
				else $bottomLevel = "The deletion of the outlet with code ".$item." failed";
			}
		}
	}else if (array_key_exists("level",$_GET)){
		// if "level" exists in $_GET the user has clicked "Find Existing" so display the relevant options
		$level = $_GET['level'];
		$bottomLevel = '<p><img src="images/divider.gif" align=center alt="blue divider"><p><table><tr><td align=left class="tc">';
		$bottomLevel .= '<select id="selection" name="selection">';
	
		if ($level == "brand")$sql = "SELECT * FROM tbl_Brands WHERE BrandCode <> '<N/A>'";
		else if ($level == "concept")$sql = "SELECT * FROM tbl_Concepts WHERE ConceptCode <> '<N/A>'";
		else if ($level == "subconcept")$sql = "SELECT * FROM tbl_SubConcepts WHERE SubConceptCode <> '<N/A>'";
		else{
			$sql = "SELECT DISTINCT OutletName, OutletCode, SubConceptName FROM tbl_Hierarchy";
			if ($_GET['outletOpt']=="optCode")$sql .= " ORDER BY SubConceptName, OutletCode";
			else $sql .= " ORDER BY SubConceptName, OutletName";
		}
		$result = odbc_exec($link, $sql);
		if ($level == "outlet"){
			if($_GET['outletOpt']=="optName"){
				while (odbc_fetch_row($result)==TRUE){
					$subconcept_name = odbc_result($result,"SubConceptName");
					if (!isset($currSCname)){
						$currSCname = $subconcept_name;
						$bottomLevel .= '<OPTGROUP LABEL="'.$subconcept_name.'">';
					}
					
					if ($currSCname != $subconcept_name){
						$bottomLevel .= '</OPTGROUP><OPTGROUP LABEL="'.$subconcept_name.'">';
						$currSCname = $subconcept_name;
					}
					$text = odbc_result($result, "outletName");
					$val = odbc_result($result, "outletCode");
					if ($val==$selection)$bottomLevel .= '<OPTION SELECTED VALUE="'.$val.'">'.$text.' ('.$val.')</OPTION>'."\n";
					else $bottomLevel .= '<OPTION VALUE="'.$val.'">'.$text.' ('.$val.')</OPTION>'."\n";
				}
			}else{
				while (odbc_fetch_row($result)==TRUE){
					$subconcept_name = odbc_result($result,"SubConceptName");
					if (!isset($currSCname)){
						$currSCname = $subconcept_name;
						$bottomLevel .= '<OPTGROUP LABEL="'.$subconcept_name.'">';
					}
					
					if ($currSCname != $subconcept_name){
						$bottomLevel .= '</OPTGROUP><OPTGROUP LABEL="'.$subconcept_name.'">';
						$currSCname = $subconcept_name;
					}
					$text = odbc_result($result, "outletName");
					$val = odbc_result($result, "outletCode");
					if ($val==$selection)$bottomLevel .= '<OPTION SELECTED VALUE="'.$val.'">'.$val.' ('.$text.')</OPTION>'."\n";
					else $bottomLevel .= '<OPTION VALUE="'.$val.'">'.$val.' ('.$text.')</OPTION>'."\n";
				}
			}
		}else{
			while (odbc_fetch_row($result)==TRUE){
				$text = odbc_result($result, $level."Name");
				$val = odbc_result($result, $level."Code");
				if ($level == "brand"){
					if ($val==$selection)$bottomLevel .= '<OPTION SELECTED VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
					else $bottomLevel .= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
				}else{
					if ($level == "concept") $parent = odbc_result($result,"BrandCode");
					else $parent = odbc_result($result,"ConceptCode");
					if ($val==$selection)$bottomLevel .= '<OPTION SELECTED VALUE="'.$val.'">'.$text.' ('.$parent.')</OPTION>'."\n";
					else $bottomLevel .= '<OPTION VALUE="'.$val.'">'.$text.' ('.$parent.')</OPTION>'."\n";
				}
			}
		}
		$bottomLevel .= '</select>';
		$bottomLevel .= '</td><td align=left class="tc">';
		$bottomLevel .= '<input type="button" value="Edit" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?level='.$level.'&selection=\'+form1.selection[selection.selectedIndex].value+isOutlet()">';
		$bottomLevel .= '</td></tr></table>';
		$bottomLevel .= '</form>';
		
		if (array_key_exists("selection",$_GET)){
			// The user has selected an individual record within the selected level
			$selection = $_GET['selection'];
			
			if ($level == "outlet"){
				$sql="SELECT * FROM tbl_hierarchy WHERE OutletCode='".$selection."'";
				$result = odbc_exec($link, $sql);
				$currBrand = odbc_result($result, "BrandCode");
				$currConcept = odbc_result($result, "ConceptCode");
				$currSubConcept = odbc_result($result, "SubConceptCode");
				$currCode = odbc_result($result, "OutletCode");
				$currName = odbc_result($result, "OutletName");
			}else if($level == "concept"){
				$sql = "SELECT * FROM tbl_Concepts WHERE ConceptCode='".$selection."'";
				$result = odbc_exec($link, $sql);
				$currBrand = odbc_result($result, "BrandCode");
				$currConcept = odbc_result($result, "ConceptCode");
				$currName = odbc_result($result, "ConceptName");
				$currCode = $currConcept;
			}else if($level == "subconcept"){
				$sql = "SELECT * FROM tbl_SubConcepts WHERE SubConceptCode='".$selection."'";
				$result = odbc_exec($link, $sql);
				$currConcept = odbc_result($result, "ConceptCode");
				$currSubConcept = odbc_result($result, "SubConceptCode");
				$currName = odbc_result($result, "SubConceptName");
				$currCode = $currSubConcept;
			}else{
				$sql = "SELECT * FROM tbl_Brands WHERE BrandCode='".$selection."'";
				$result = odbc_exec($link, $sql);
				$currBrand = odbc_result($result, "BrandCode");
				$currName = odbc_result($result, "BrandName");
				$currCode = $currBrand;
			}
			$bottomLevel .= '<p><img src="images/divider.gif" align=center alt="blue divider">';
			$bottomLevel .= '<FORM NAME="details" method="POST" action="?level='.$level.'&save='.$selection.'">';
			$bottomLevel .= '<table><tr><td align=left class="tc">';
			$bottomLevel .= '<label for="name">Name:</label>';
			$bottomLevel .= '</td><td align=left class="tc">';
			$bottomLevel .= '<input type=text name="name" id="name" value="'.$currName.'">';
			$bottomLevel .= '</td></tr><tr><td align=left class="tc">';
			$bottomLevel .= '<label for="code">Code:</label>';
			$bottomLevel .= '</td><td align=left class="tc">';
			$bottomLevel .= '<input type=text name="code" id="code" value="'.$currCode.'"><br>';
			$bottomLevel .= '</td></tr><tr><td align=left class="tc">';
			
			if ($level=="outlet"){
				$bottomLevel .= '<label for="subconcept">Sub-Concept:</label>';
				$bottomLevel .= '</td><td align=left class="tc">';
				$bottomLevel .= '<select name="subconcept" id="subconcept">';
				$sql = "SELECT * FROM tbl_SubConcepts";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$text = odbc_result($result, "SubConceptName");
					$val = odbc_result($result, "SubConceptCode");
					if ($val==$currSubConcept)$bottomLevel.= '<OPTION SELECTED VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
					else $bottomLevel.= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
				}
				$bottomLevel .= '</select><br>';
			}
			
			if ($level=="subconcept"){
				$bottomLevel .= '<label for="concept">Concept:</label>';
				$bottomLevel .= '</td><td align=left class="tc">';
				$bottomLevel .= '<select name="concept" id="concept">';
				$sql = "SELECT * FROM tbl_Concepts";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$text = odbc_result($result, "ConceptName");
					$val = odbc_result($result, "ConceptCode");
					if ($val==$currConcept)$bottomLevel.= '<OPTION SELECTED VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
					else $bottomLevel.= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
				}
				$bottomLevel .= '</select>';
			}
			
			if ($level=="concept"){
				$bottomLevel .= '<label for="brand">Brand: </label>';
				$bottomLevel .= '</td><td align=left class="tc">';
				$bottomLevel .= '<select name="brand" id="brand">';
				$sql = "SELECT * FROM tbl_Brands";
				$result = odbc_exec($link, $sql);
				while (odbc_fetch_row($result)==TRUE){
					$text = odbc_result($result, "BrandName");
					$val = odbc_result($result, "BrandCode");
					if ($val==$currBrand)$bottomLevel.= '<OPTION SELECTED VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
					else $bottomLevel.= '<OPTION VALUE="'.$val.'">'.$text.'</OPTION>'."\n";
				}
				$bottomLevel .= '</select>';
			}
			$bottomLevel .= '</td></tr></table>';
			$bottomLevel .= '<p>';
			
			$bottomLevel .= '<table><tr><td align=center class="tc">';
			$bottomLevel .= '<input type="submit" value="Save">';
			$bottomLevel .= '</td><td align=center class="tc">';
			$bottomLevel .= '<input type="button" value="Cancel" onClick=window.location=\'manage.php\'>';
			$bottomLevel .= '</td><td align=center class="tc">';
			$bottomLevel .= '<input type="reset" value="Reset">';
			$bottomLevel .= '</td><td align=center class="tc">';
			$bottomLevel .= '<input type="button" value="Delete" onClick="toggleDeletion()" id="delToggle" class="toggleUp">';
			$bottomLevel .= '</td></tr></table>';
			
			$bottomLevel .= '<br><div id="deletionDiv" style="display:none; border:1px solid #090858; width:50%">';
			$bottomLevel .= '<b>How would you like to delete this '.$level.'?</b>';
			$bottomLevel .= '<p><input CHECKED type="radio" name="delRecords" value="all" id="all"><label for="all">Delete ALL records containing this '.$level.'</label>';
			$bottomLevel .= '<br><input type="radio" name="delRecords" value="none" id="none"><label for="none">Set all entries for this '.$level.' to UNALLOCATED</label>';
			$bottomLevel .= '<p><input type="button" value="Confirm Deletion" onClick="window.location=\'?level='.$level.'&delete='.$selection.'&delRecords=\'+checkRecords()"></div></form>';
		}
	}
	
	// TOP CONTENT
	$topLevel = '<FORM NAME="form1" method="POST">';
	$topLevel .= '<table><tr><td class="tc">';
	$topLevel .= '<select id="level" name="level" onChange="checkSelect()">';
		$topLevel .= '<OPTION '. ($level=="brand"?"SELECTED ":"").'value="brand">Brand</OPTION>'."\n";
		$topLevel .= '<option '. ($level=="concept"?"SELECTED ":"").'value="concept">Concept</option>'."\n";
		$topLevel .= '<option '. ($level=="subconcept"?"SELECTED ":"").'value="subconcept">Sub-Concept</option>'."\n";
		$topLevel .= '<option '. ($level=="outlet"?"SELECTED ":"").'value="outlet">Outlet</option>'."\n";
	$topLevel .= '</select>';
	$topLevel .= '</td><td class="tc">';
	$topLevel .= '<input type="button" value="Find Existing" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?level=\'+form1.level[level.selectedIndex].value+isOutlet()">';
	$topLevel .= '</td><td class="tc">';
	$topLevel .= '<input type="button" value="Add New" onmouseover="get(\'levelOpt\').style.display=\'none\'" onmouseout="checkSelect()" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?add=\'+form1.level[level.selectedIndex].value">';
	$topLevel .= '</td></tr>';
	$topLevel .= '<tr><td colspan=3 align="center" class="tc"><div id="levelOpt"'.($level!="outlet"?"style=display:none":"").'>';
	$topLevel .= '<input '.($_GET['outletOpt']=="optName"?"CHECKED ":(!isset($_GET['outletOpt'])?"CHECKED ":"")).'type="radio" name="outletOpt" value="optName" id="optName"><label for="optName">By Outlet Name</label>';
	$topLevel .= '<input '.($_GET['outletOpt']=="optCode"?"CHECKED ":"").'type="radio" name="outletOpt" value="optCode" id="optCode"><label for="optCode">By Outlet Code</label></div></td></table>';
	//$topLevel .= '<input type="button" value="Find New Outlets" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?unassigned=true\'">';
	// END TOP CONTENT
?>

<html><head>
	<title>Hierarchy Management</title>
	<script language="javaScript" type="text/javascript" src="javascript.js"></script>
	<link href="style.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body onload="checkSelect()">
	<div id="content">
	<!-- This table structures the page's title and link --> 
	<table width=100%>
		<tr>
			<td align=center>
				<img src="images/logo.gif" align=top alt="Whitbread logo">
				<br><br>
				<a href="index.php">Reporting Page</a>
				<p><a href="?unassigned">View New Outlets</a>
			</td>
			<td align=center>
				<h1>Food Delivery Reporting</h1>
				<h2>Hierarchy Management</h2>
			</td>
		</tr>
	</table>
	<p>
	<img src="images/divider.gif" align=center alt="blue divider"><p>
	
	<center>
	<?PHP echo $topLevel ?>
	<?PHP echo $bottomLevel ?>
	</center>
	</div>
</body>

</html>