<?PHP
	//print_r($_POST);
	include('includes/links.php');

	if (isset($_POST['code'])){
		$code = $_POST['code'];
		$hiddenOptions = $_POST['hiddenOptions'];
		$hiddenCodes = $_POST['hiddenCodes'];
		if ($code <> ''){
			$enteredCodes = explode(';',$code);
			foreach ($enteredCodes as $code){
				if ($code=='')continue;
				$sql = "SELECT DISTINCT Top 1 WBCode,Description FROM ViewWBCodes WHERE WBCode='".$code."'";
				$result = odbc_exec($link, $sql);
				if (!strstr($hiddenCodes,$code)){
					if(odbc_num_rows($result)>0){
						$string = odbc_result($result,"WBCode");
						$string .= " (".odbc_result($result,"Description").")";
						
						if (!strchr($string,'""')==FALSE){
							$string = str_replace('""','inch',$string);
							$string = str_replace('"','',$string);
						}
						
						if (strlen($hiddenOptions) == 0){
							$hiddenOptions = $string;
							$hiddenCodes = odbc_result($result,"WBCode");
						}else{
							$hiddenOptions .= ",".$string;
							$hiddenCodes .= ",".odbc_result($result,"WBCode");
						}
					}else $message .= "The Code <b>".$code."</b> was not found<br>";
				}else $message .= "The Code <b>".$code."</b> is already in the list<br>";
			}
		}
	}else $onload = " onLoad=\"getExistingCodes()\"";
		
	$codes = explode(',',$hiddenOptions);
	if ($codes[0]<>''){
		foreach($codes as $c){
			$options .= "<option value='".$c."'>".$c."</option>";
		}
	}
?>

<HTML>
<HEAD>
	<script language="javaScript" type="text/javascript" src="javascript.js"></script>
	<link href="style.css" rel="stylesheet" type="text/css" media="screen">
	<TITLE>Food Delivery Reporting For Whitbread Supply Chain</TITLE>
</HEAD>
<BODY<?PHP echo $onload?>>
	<form method="POST">
		<table cellspacing=5>
		<tr>
			<td><input type="text" size=35 id="code" name="code" value=""></td>
			<td align="right"><input type="submit" id="addCode" name="addCode" value="Add Codes To List"></td>
		</tr>
		<tr><td colspan=2><?PHP echo $message?></td></tr>
		<!--<p>-->
		<tr><td colspan=2><select multiple size=5 id="codeList" name="codeList" style="width:400px"><?PHP echo $options ?></select></td></tr>
		<tr><td><input type="button" id="remCode" name="remCode" value="Remove Selected Codes" onclick="remCodes()"></td>
		<td align="right">
			<input type="hidden" id="hiddenOptions" name="hiddenOptions" value="<?PHP echo $hiddenOptions ?>">
			<input type="hidden" id="hiddenCodes" name="hiddenCodes" value="<?PHP echo $hiddenCodes ?>">
			<!--<p>-->
			<input type="button" id="done" name="done" value="Click Here To Finish" onClick="doCodes()" style="font-weight:bold;">
		</td></tr>
	</form>
</Body>
</html>