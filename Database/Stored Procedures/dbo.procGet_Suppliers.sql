SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		change++
-- Create date: 15/08/2011
-- Description:	Retrieve List of suppliers
-- =============================================
CREATE PROCEDURE [dbo].[procGet_Suppliers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT
		Supplier
	FROM
		Tbl_Dishcost
	WHERE
		Supplier IS NOT NULL
	ORDER BY
		Supplier
END


GO
