SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		change++ (Matt Taylor)
-- Create date: 20/05/11
-- Description:	Populate the hierarchy table
-- =============================================
CREATE PROCEDURE [dbo].[procPopulate_Hierarchy] 
	@debug int = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF(@debug > 0)
	BEGIN
		DELETE FROM Tbl_Hierarchy
		WHERE OutletCode IN (SELECT DISTINCT Outlet_Code_7 FROM dbo.Tbl_Hierarchy_WHR_Import)
			OR OutletCode IN (SELECT DISTINCT Outlet_Code_7 FROM dbo.Tbl_Hierarchy_Costa_Import)

		INSERT INTO Tbl_Hierarchy
		SELECT
			d.Brand as BrandCode
			,b.BrandName
			,d.Concept_Code
			,d.Concept
			,d.SubConcept_Code
			,d.SubConcept
			,d.Outlet_Code_7 as OutletCode
			,d.Location_Name as OutletName
			,d.Outlet_Code_8 as OutletCode8
		FROM (
				SELECT 
					Outlet_Code_7
					, Outlet_Code_8
					, Location_Name
					, Brand
					, Concept_Code
					, Concept
					, SubConcept_Code
					, SubConcept
				FROM dbo.Tbl_Hierarchy_WHR_Import
				UNION
				SELECT 
					Outlet_Code_7
					, Outlet_Code_8
					, Outlet_Name
					, Brand
					, Concept_Code
					, 'Costa Coffee' AS Concept
					, SubConcept_Code
					, CASE SubConcept_Code
						WHEN 'CC' THEN 'Costa Equity'
						WHEN 'IF' THEN 'Costa Individual Franchise'
						WHEN 'CF' THEN 'Costa Corporate Franchise'
						ELSE NULL
						END as SubConcept
				FROM dbo.Tbl_Hierarchy_Costa_Import

			) d
			INNER JOIN Tbl_Brands b
			ON d.Brand = b.BrandCode
		WHERE outlet_code_7 IS NOT NULL
			AND Concept_Code <> 'IGNORE'
			--AND Concept_Code <> 'ERROR'
			AND SubConcept_Code <> 'ERROR'
			AND Outlet_Code_7 <> '2172851'

		TRUNCATE TABLE Tbl_Concepts

		INSERT INTO Tbl_Concepts
		SELECT DISTINCT
			ConceptCode
			,ConceptName
			,BrandCode
		FROM Tbl_Hierarchy
		WHERE 
			SubConceptCode <> 'ERROR'
			--AND ConceptCode <> 'ERROR'
			AND ConceptCode <> 'IGNORE'

		TRUNCATE TABLE Tbl_SubConcepts

		INSERT INTO Tbl_SubConcepts
		SELECT DISTINCT
			SubConceptCode
			,SubConceptName
			,ConceptCode
		FROM Tbl_Hierarchy
		WHERE 
			SubConceptCode <> 'ERROR'
			--AND ConceptCode <> 'ERROR'
			AND ConceptCode <> 'IGNORE'
	END
	ELSE
	BEGIN
		DELETE FROM Tbl_Hierarchy
		WHERE OutletCode IN (SELECT DISTINCT Outlet_Code_7 FROM dbo.Tbl_Hierarchy_WHR_Import)
			OR OutletCode IN (SELECT DISTINCT Outlet_Code_7 FROM dbo.Tbl_Hierarchy_Costa_Import)

		INSERT INTO Tbl_Hierarchy
		SELECT
			d.Brand as BrandCode
			,b.BrandName
			,d.Concept_Code
			,d.Concept
			,d.SubConcept_Code
			,d.SubConcept
			,d.Outlet_Code_7 as OutletCode
			,d.Location_Name as OutletName
			,d.Outlet_Code_8 as OutletCode8
		FROM (
				SELECT Outlet_Code_7, Outlet_Code_8, Location_Name, Brand, Concept, SubConcept, Concept_Code, SubConcept_Code FROM dbo.Tbl_Hierarchy_WHR_Import
				UNION
				SELECT Outlet_Code_7, Outlet_Code_8, Outlet_Name, Brand, Area_Code, Region_Code, Concept_Code, SubConcept_Code FROM dbo.Tbl_Hierarchy_Costa_Import
			) d
			INNER JOIN Tbl_Brands b
			ON d.Brand = b.BrandCode
		WHERE outlet_code_7 IS NOT NULL
			AND Concept_Code <> 'IGNORE'
			--AND Concept_Code <> 'ERROR'
			AND SubConcept_Code <> 'ERROR'
			AND Outlet_Code_7 <> '2172851'

		TRUNCATE TABLE Tbl_Concepts

		INSERT INTO Tbl_Concepts
		SELECT DISTINCT
			ConceptCode
			,ConceptName
			,BrandCode
		FROM Tbl_Hierarchy
		WHERE 
			SubConceptCode <> 'ERROR'
			--AND ConceptCode <> 'ERROR'
			AND ConceptCode <> 'IGNORE'

		TRUNCATE TABLE Tbl_SubConcepts

		INSERT INTO Tbl_SubConcepts
		SELECT DISTINCT
			SubConceptCode
			,SubConceptName
			,ConceptCode
		FROM Tbl_Hierarchy
		WHERE 
			SubConceptCode <> 'ERROR'
			--AND ConceptCode <> 'ERROR'
			AND ConceptCode <> 'IGNORE'

	END
END








GO
