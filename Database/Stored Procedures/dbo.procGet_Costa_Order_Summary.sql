SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		change++
-- Create date: 02/02/2012
-- Description:	Get the Costa Order Summary Report
-- =============================================
CREATE PROCEDURE [dbo].[procGet_Costa_Order_Summary]
	@pInterval VARCHAR(50)
	, @pStartDate VARCHAR(10) = NULL
	, @pEndDate VARCHAR(10) = NULL
	, @pWBCodes VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE
		@StartDate VARCHAR(10)
		, @EndDate VARCHAR(10)
		, @cols NVARCHAR(2000)
		, @query NVARCHAR(4000)
	--SET @ShowByDay = ''

	IF @pInterval = 'week'
	BEGIN
		SELECT @StartDate = Week_Seq FROM Calendar WHERE Week_Name = CONVERT(VARCHAR(10),CONVERT(DATETIME,@pStartDate,103),103)
		SELECT @EndDate = Week_Seq FROM Calendar WHERE Week_Name =CONVERT(VARCHAR(10),CONVERT(DATETIME,@pEndDate,103),103)
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(Week_Seq) FROM Calendar WHERE Period_Seq = @pStartDate
		SELECT @EndDate = MAX(Week_Seq) FROM Calendar WHERE Period_Seq = @pEndDate
	END

	--DECLARE @cols NVARCHAR(2000)
	SELECT  @cols = COALESCE(@cols + ',[' + Period + ']', '[' + Period + ']')
	FROM    (
		SELECT 'P' + [Period_Name] + ' ' + Year_Name [Period], MIN(Week_seq)[Seq]
		FROM Calendar
		WHERE Week_Seq BETWEEN @StartDate AND @EndDate --Week_Code >= '20100311'
		GROUP BY [Period_Name], Year_Name
	) Cal
	ORDER BY Seq
	--PRINT @cols

	--DECLARE @query NVARCHAR(4000)
	SET @query = N'SELECT Subconcept, Site, WBCode, Description, '+
	@cols +'
	FROM (
		SELECT 
			SubConcept
			, ''P'' + [Period_Name] + '' ''+Year_Name [Period]
			, h.OutletCode + '' / '' + h.OutletCode8 + '' / '' + h. OutletName [Site]
			, d.WBCode
			, d.Description
			, SUM(ISNULL(Delivered,0)) [Delivered]
		FROM    
			Tbl_SOD_Data
			INNER JOIN dbo.Calendar 
			ON DeliveryDate 
				BETWEEN Week_Start_Date
				AND Week_End_Date
			INNER JOIN Tbl_Hierarchy h 
			ON Tbl_SOD_Data.Outlet = h.OutletCode
			INNER JOIN tbl_dishcost d 
			ON d.wbcode = Tbl_SOD_Data.wbcode
		WHERE
			Week_Seq BETWEEN ' + @StartDate + ' AND ' + @EndDate + '
			AND Subconcept in (''CF'',''CC'')'
			IF @pWBCodes IS NOT NULL 
				SET @query = @query + ' AND d.WBCode IN (''' + @pWBCodes + ''')'
	SET @query = @query + '
		GROUP BY
			SubConcept
			, ''P'' + [Period_Name] + '' ''+Year_Name
			, h.OutletCode + '' / '' + h.OutletCode8 + '' / '' + h. OutletName
			, d.WBCode
			, d.Description
	) p
	PIVOT (
		sum(Delivered)
		FOR Period IN
		( '+ @cols + ' )
	) AS pvt;'
	--ORDER BY tID;'
	PRINT @query
	EXEC (@query)
END


GO
