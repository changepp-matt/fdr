SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Andy Best
-- Create date: 18/07/2008
-- Description:	Delete SOD Date Older Than 18 Months
-- =============================================
CREATE PROCEDURE [dbo].[procDeleteOldData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @cutDate datetime
	set @cutDate = (SELECT Top 1 Convert(datetime,week_name,103) FROM viewLast18Months)
	DELETE FROM tbl_sod_data WHERE CONVERT(datetime,deliverydate,103) < @cutDate
END

GO
