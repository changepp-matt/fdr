SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		change++ (MT)
-- Create date: 19/05/2011
-- Description:	As part of CCR245/1 the imported Costa sites need to be updated to use the 7 digit outet codes
-- =============================================
CREATE PROCEDURE [dbo].[procUpdateHierarchy_CostaCodes] 
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE 
		Tbl_Hierarchy_Costa_Import
	SET 
		Outlet_Code_7 = t.Outlet_Code_7
	FROM 
		Tbl_Hierarchy_Costa_Import h
		INNER JOIN Tbl_Hierarchy_Costa_Translation t
		ON h.Outlet_Code_8 = t.Outlet_Code_8
END

GO
