SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		change++
-- Create date: 29/03/2012
-- Description:	Import Dishcost data from import table
-- =============================================
CREATE PROCEDURE [dbo].[procImport_Dishcost_Data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO Tbl_Dishcost
	SELECT
		*
	FROM
		Tbl_Dishcost_Import
	WHERE
		WBCode IS NOT NULL

END

GO
