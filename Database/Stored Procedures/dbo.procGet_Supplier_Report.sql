SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		change++
-- Create date: 15/08/2011
-- Description:	Retrieves the data for the supplier Report
-- =============================================
CREATE PROCEDURE [dbo].[procGet_Supplier_Report]
	-- Add the parameters for the stored procedure here
	@pSupplier VARCHAR(250)
	, @pInterval VARCHAR(50)
	, @pStartDate VARCHAR(10) = NULL
	, @pEndDate VARCHAR(10) = NULL
	, @pWBCodes VARCHAR(MAX) = NULL
	, @pShowByDay BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE
		@StartDate VARCHAR(10)
		, @EndDate VARCHAR(10)
		, @SQL VARCHAR(MAX)
		, @ShowByDay VARCHAR(50)
	SET @ShowByDay = ''
	
	-- SET UP COLUMN NAMES
	--DECLARE @cols VARCHAR(MAX)
	--SELECT  @cols =  COALESCE(@cols + ',[' + Description + ']','[' + Description + ']')
	--FROM    Tbl_Dishcost
	--WHERE Description IS NOT NULL AND Supplier = @pSupplier
	--ORDER BY COALESCE(@cols + ',[' + Description + ']','[' + Description + ']')
	
	DECLARE
		@cols VARCHAR(MAX)
		, @colHeaders VARCHAR(MAX)
	
	IF @pWBCodes IS NOT NULL
	BEGIN
		SET @pWBCodes = ''''+REPLACE(@pWBCodes,',',''',''')+''''
	END

	SELECT 
		@cols = COALESCE(@cols + ',[' + [Description] + ']','[' + [Description] + ']')
		, @colHeaders = COALESCE(@colHeaders + ',ISNULL([' + [Description] + '],0)[' + [Description] + ']','ISNULL([' + [Description] + '],0)[' + [Description] + ']')
	FROM    (
		SELECT DISTINCT
			[Description]
		FROM 
			Tbl_Dishcost
		WHERE 
			[Description] IS NOT NULL
			AND Supplier = @pSupplier
			--AND (WBCode IN (@pWBCodes) OR @pWBCodes IS NULL)
	) d
	ORDER BY
		[Description]
	
	IF @pInterval = 'week'
	BEGIN
		SELECT 
			@StartDate = CONVERT(VARCHAR(10),DATEADD(dd,-6,CONVERT(DATETIME,@pStartDate,103)),103)
			, @EndDate = CONVERT(VARCHAR(10),CONVERT(DATETIME,@pEndDate,103),103)
	END
	ELSE
	BEGIN
		SELECT 
			@StartDate = CONVERT(VARCHAR(10),MIN(DATEADD(dd, -6, CONVERT(DATETIME,Week_Name,103))),103)
			, @EndDate = CONVERT(VARCHAR(10),MAX(CONVERT(DATETIME,Week_Name,103)),103)
		FROM 
			viewLast18Months
		WHERE 
			Period_Seq IN (@pStartDate, @pEndDate)
	END
	
	IF @pShowByDay = 1 SET @ShowByDay = '[Delivery Date],'

    SET @SQL = 'SELECT ' + @ShowByDay + '
		Site, Supplier, ' + @colHeaders + '
	FROM (
		SELECT 
			'
			IF @pShowByDay = 1 SET @SQL = @SQL + 'CONVERT(VARCHAR(10),DeliveryDate,103)[Delivery Date],'
			SET @SQL = @SQL + 'OutletCode + '' / '' + OutletName +  '' /  '' + ISNULL(OutletCode8,''-'') [Site]
			, Description
			, Supplier
			, ISNULL(Delivered,0) [Total]
		FROM
			dbo.tbl_SOD_Data s
			INNER JOIN Tbl_Hierarchy h
			ON s.Outlet = h.OutletCode
			INNER JOIN Tbl_Dishcost d
			ON s.WBCode = d.WBCode
			AND d.Concept = h.conceptcode
		WHERE
			Supplier = ''' + @pSupplier + ''''
			IF @pWBCodes IS NOT NULL 
				SET @SQL = @SQL + ' AND d.WBCode IN (' + @pWBCodes + ')'
			SET @SQL = @SQL + ' AND (DeliveryDate BETWEEN CONVERT(DATETIME,''' + @StartDate + ''',103) AND CONVERT(DATETIME,''' + @EndDate + ''',103))'+ --AND DeliveryDate BETWEEN CONVERT(DATETIME,''' + @StartDate + ''',103) AND CONVERT(DATETIME,''' + @EndDate + ''',103)*/
	') pivData PIVOT (
		SUM(Total) FOR Description IN ('+@cols+')
	) pt'
	
	PRINT @SQL
	EXEC (@SQL)
END






GO
