SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewLast18Months]
AS
SELECT TOP 100 PERCENT
	Week_Name, 
	Period_Seq, 
	Week_Seq
FROM         
	dbo.Calendar
WHERE
	Week_Seq BETWEEN
	--545 AND
(
	SELECT MIN(Week_Seq) FROM dbo.Calendar WHERE
	Period_Seq = (
		SELECT TOP 1
			Period_Seq - 18
		FROM
			dbo.Calendar AS Calendar_3
		WHERE
			(CONVERT(datetime, Week_Name, 103) <= GETDATE())
		ORDER BY
			Period_Seq DESC,
			Week_Seq DESC
	)
)
AND
	(SELECT TOP (1) Week_Seq FROM  dbo.Calendar WHERE (CONVERT(datetime, Week_Name, 103)) >= CONVERT(datetime,CONVERT(VARCHAR,GETDATE())) ORDER BY Week_Seq)
	--(SELECT TOP (1) Week_Seq FROM  dbo.Calendar WHERE CONVERT(datetime, Week_Name, 103) = CONVERT(datetime,CONVERT(VARCHAR(10),GETDATE(),103)))
/*
SELECT TOP (100) PERCENT
	Week_Name, 
	Period_Seq, 
	Week_Seq
FROM         
	dbo.Calendar
WHERE (
			Week_Seq BETWEEN (
							SELECT     TOP (1) Week_Seq
							FROM          dbo.Calendar AS Calendar_1
							WHERE      (
										Period_Seq = (
													SELECT     TOP (1) Period_Seq - 18 AS Expr1
													FROM          dbo.Calendar AS Calendar_3
													WHERE      (CONVERT(datetime, Week_Name, 103) < GETDATE())
													ORDER BY Period_Seq DESC
													)
										) AND (
										Week_In_Period = (
													SELECT     TOP (1) Week_In_Period
													FROM          dbo.Calendar AS Calendar_2
													WHERE      (CONVERT(datetime, Week_Name, 103) < GETDATE())
													ORDER BY Week_Seq DESC
													)
										)
			) AND (
					SELECT     TOP (1) Week_Seq
					FROM          dbo.Calendar AS Calendar_1
					WHERE      (CONVERT(datetime, Week_Name, 103) >= GETDATE())
					ORDER BY Week_Seq
			)
		)

*/

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[26] 4[35] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Calendar"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 115
               Right = 197
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'viewLast18Months', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'viewLast18Months', NULL, NULL
GO
