CREATE TABLE [dbo].[tbl_Concepts]
(
[ConceptCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ConceptName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BrandCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
