CREATE TABLE [dbo].[Tbl_Dishcost_Import]
(
[WBCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ConceptProductCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Concept] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Quantity] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Supplier] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UOM] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UOM_Contents] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ActPrice] [decimal] (18, 2) NULL,
[StdPrice] [decimal] (18, 2) NULL,
[ProductGroup] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ActPriceDate] [datetime] NULL,
[StdPriceDate] [datetime] NULL,
[Status] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DispatchCriteria] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MinShelfLife] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
