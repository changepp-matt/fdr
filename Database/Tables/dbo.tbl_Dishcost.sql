CREATE TABLE [dbo].[tbl_Dishcost]
(
[WBCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ConceptProductCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Concept] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Quantity] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Supplier] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UOM] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UOM_Contents] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ActPrice] [decimal] (18, 2) NULL,
[StdPrice] [decimal] (18, 2) NULL,
[ProductGroup] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ActPriceDate] [datetime] NULL,
[StdPriceDate] [datetime] NULL,
[Status] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DispatchCriteria] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MinShelfLife] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Dishcost] ADD CONSTRAINT [PK_tbl_Dishcost] PRIMARY KEY CLUSTERED  ([WBCode], [ConceptProductCode], [Concept]) ON [PRIMARY]
GO
