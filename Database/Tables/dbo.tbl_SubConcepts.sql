CREATE TABLE [dbo].[tbl_SubConcepts]
(
[SubConceptCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SubConceptName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ConceptCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
