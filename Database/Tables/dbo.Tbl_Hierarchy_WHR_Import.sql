CREATE TABLE [dbo].[Tbl_Hierarchy_WHR_Import]
(
[Outlet_Code_7] [varchar] (7) COLLATE Latin1_General_CI_AS NULL,
[Outlet_Code_8] [varchar] (8) COLLATE Latin1_General_CI_AS NULL,
[Location_Name] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Brand] AS ('WHR'),
[Concept] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SubConcept] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Concept_Code] AS (case [Concept] when 'Premier Inn' then 'TI' when 'Beefeater' then 'BE' when 'BrewersFayre' then 'IN' when 'Hub By Premier Inn' then 'HU' when 'Costa' then 'IGNORE' else 'ERROR' end),
[SubConcept_Code] AS (case [SubConcept] when 'Premier Travel Inn' then 'TI' when 'Beefeater' then 'BE' when 'BrewersFayre' then 'BF' when 'Hub By Premier Inn' then 'HU' when 'Table Table' then 'TT' when 'Taybarns' then 'TB' when 'cookhouse' then 'CH' when 'Bar + Block' then 'BB' when 'Whitbread Inns' then 'WI' when 'Costa' then 'IGNORE' else 'ERROR' end)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
