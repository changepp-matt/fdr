CREATE TABLE [dbo].[Calendar]
(
[Week_Seq] [int] NOT NULL,
[Week_Code] [int] NOT NULL,
[Week_Name] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Week_Start_Date] [datetime] NULL,
[Week_End_Date] [datetime] NULL,
[Week_In_Period] [int] NOT NULL,
[Week_No] [int] NOT NULL,
[Period_Code] [int] NOT NULL,
[Period_Name] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Period_Seq] [int] NOT NULL,
[Year_Code] [int] NOT NULL,
[Year_Name] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Period_Num] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Calendar] ADD CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED  ([Week_Seq]) ON [PRIMARY]
GO
