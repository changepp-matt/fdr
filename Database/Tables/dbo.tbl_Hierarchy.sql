CREATE TABLE [dbo].[tbl_Hierarchy]
(
[BrandCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BrandName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ConceptCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ConceptName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SubConceptCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SubConceptName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OutletCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[OutletName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OutletCode8] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Hierarchy] ADD CONSTRAINT [PK_tbl_Hierarchy] PRIMARY KEY CLUSTERED  ([OutletCode]) ON [PRIMARY]
GO
