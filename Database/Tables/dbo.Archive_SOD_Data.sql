CREATE TABLE [dbo].[Archive_SOD_Data]
(
[Concept] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Subconcept] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Depot] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RouteCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Outlet] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DeliveryDate] [datetime] NULL,
[OrderRef] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WBCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ConceptProductCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Demand] [int] NULL,
[Ordered] [int] NULL,
[Picked] [int] NULL,
[Delivered] [int] NULL,
[ReasonCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
