CREATE TABLE [dbo].[tbl_Brands]
(
[BrandCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BrandName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
