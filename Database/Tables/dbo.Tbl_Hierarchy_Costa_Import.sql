CREATE TABLE [dbo].[Tbl_Hierarchy_Costa_Import]
(
[Brand] AS ('Costa'),
[SubBrand_Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SubBrand_Code] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Region_Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Region_Code] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Area_Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Area_Code] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Outlet_Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Outlet_Code_8] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Outlet_Code_7] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[L4L_Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[L4L_Code] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Concept_Code] AS ('CC'),
[SubConcept_Code] AS (case  when [Area_Code]='RCOFRUKI' OR [Area_Code]='RCOFRUKW' OR [Region_Code]='RCOCSTOT' OR [Region_Code]='RCCOATOT' then 'CF' when [Region_Code]='RCOFRAUK' then 'IF' else 'CC' end)
) ON [PRIMARY]
GO
